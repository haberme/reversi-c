/*
 * IPPortReader.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/IPPortReader.h"

#define TEST true

int stringToNumber(string stringNumber)
{
	int number = 0;
	// start from the end and up by base 10.
	int currentPosition = strlen(stringNumber.c_str()) - 1;
	// check if we got empty string.
	if (stringNumber[currentPosition] == '\0')
		return -1;
	int check;
	for (int product = 1; currentPosition > -1;product *= 10,currentPosition--)
	{
		// we used the fact that in ascii we can get digit from letter by
		// strating from '0' i.e. letter -'0' which mean everything between 0
		// and 9 is a digit.
		check = stringNumber[currentPosition] - '0';
		if ((check > -1) and (check < 10))
			number += (check * product);
		else
			return -1;
	}

	return number;
}

IPPortSettings readFile(const char *path)
{
	struct IPPortSettings settings;
	string line;

	ifstream inputFile(path);

	// for google tests
	// stringstream inputFile;
	// inputFile << path;

	// file not existce
	if (inputFile.fail())
		throw FileNotExist();

	// get the ip.
	settings.IP = "";
	getline(inputFile, line);
	// chekc ip
	int lineSize = strlen(line.c_str());
	int currentPosition  = 0;
	int numberCheck;

	// check that the 4 ip section are within the limits.
	for (int i = 0; i < 4; i++)
	{
		// less then 4 section
		if (lineSize == currentPosition)
			throw IPError();
		string stringCheck = "";

		// get the current section for check.
		while ((line[currentPosition] != '.') and (lineSize !=currentPosition))
		{
			stringCheck += line[currentPosition];
			currentPosition++;
		}
		currentPosition++;
		numberCheck = stringToNumber(stringCheck);

		if ((numberCheck < IP_LOWER_LIMMIT) or
			(numberCheck > IP_UPPER_LIMMIT) or (numberCheck == -1))
			throw IPError();
	}

	// check that there are more then 4 section.
	if (lineSize != (currentPosition - 1))
		throw IPError();

	settings.IP += line;

	// check that we have a port entry.
	if (inputFile.eof())
		throw PortError();

	// get the port.
	getline(inputFile, line);
	int port = stringToNumber(line);
	if ((port >= PORT_LOWER_LIMMIT) and (port <= PORT_UPPER_LIMMIT))
		settings.port = port;
	else
		throw PortError();

	inputFile.close();
	return settings;
}
