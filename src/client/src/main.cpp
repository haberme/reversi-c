/*
 * main.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include <iostream>
#include "../include/ConsolGame.h"
#include "../include/RemoteGame.h"
#include "../include/IPPortReader.h"

#include <exception>

#define ROW_SIZE (8)
#define COLUMN_SIZE (8)

using namespace std;
/**
 * Function Name: getUserInput
 * Description: gets player 1 selection for his opponent (local player or AI player).
 * Parameters:void.
 * Return Value:opponent - if 1 is selected opponent will be local human player
 *  if 2 is selected opponent will be AI player,if 3 is selected play a remote game.
 */
int getUserInput()
{
	unsigned int opponent = 0;
	int end = 1;
	cout << "         Welcome to Reversi game:" << endl << endl;
	do
	{
		cout << "please choose your opponent:" << endl
				<< "press 1 to play against a Human player. " << endl
				<< "press 2 to play against a AI player." << endl
				<< "press 3 to play against a Remote player." << endl;
		cin >> opponent;
		if (!cin.fail())
		{
			if ((opponent == 1) || (opponent == 2) || (opponent == 3))
			{
				end = 0;
			}
			else
			{
				cout << "Error: wrong input, please choose between 1 or 2."
						<< endl << endl;
			}
		}
		else
		{
			cout << "Error: wrong input, only numbers are allowed." << endl
					<< endl;
		}
		cin.clear();
		cin.ignore(10000, '\n');
	} while (end == 1);
	return (opponent);
}
/**
 * Function Name: menu
 * Description: shows the player 2 options (local player or AI player)
 * and creates the game.
 * Parameters:void.
 * Return Value:void.
 */
void menu()
{
	unsigned int opponent = getUserInput();
	Player *p1;
	Player *p2;
	Game *g;
	try
	{
		if (opponent == 3)
		{
			//create a remote player game.
			try
			{
				p2 = new LocalPlayer(player_2);
			} catch (const char *msg)
			{
				cout
						<< "Error: in client::main::menu failed creating LocalPlayer, reason:"
						<< msg << endl;
				return;
			}
			try
			{
				// get ip and port from file
				struct IPPortSettings settings;
				const char* path="client_config.txt";
    				settings = readFile(path);
				const char* server_ip =settings.IP.c_str();
				const int server_port =settings.port;
				p1 = new RemotePlayer(server_ip, server_port,p2);
			} catch (const char *msg)
			{
				delete p2;
				cout
						<< "Error: in client::main::menu failed creating RemotePlayer, reason:"
						<< msg << endl;
				return;
			}
			try
			{
				g = new RemoteGame(ROW_SIZE, COLUMN_SIZE, p1, p2);
				g->play();
			} catch (const char *msg)
			{
				delete g;
				delete p1;
				delete p2;
				cout
						<< "Error: in client::main::menu failed running RemoteGame, reason:"
						<< msg << endl;
				return;
			}
		}
		else
		{ //create a local game based on chosen opponent.
			p1 = new LocalPlayer(player_1);
			if (opponent == 1)
			{
				p2 = new LocalPlayer(player_2);
			}
			if (opponent == 2)
			{
				p2 = new AIPlayer(player_2);
			}
			try
			{
				g = new ConsolGame(ROW_SIZE, COLUMN_SIZE, p1, p2);
				g->play();
			} catch (std::bad_alloc & e)
			{
				delete p1;
				delete p2;
				throw("Error:failed to create Game.");
				return;
			}
		}
	} catch (std::bad_alloc &e)
	{
		throw("Error:failed to create game.");
	}
	delete p1;
	delete p2;
	delete g;
}
/**
 * Function Name: main
 * Description: the game main function calls for the game menu.
 * Parameters:void.
 * Return Value:void.
 */

int main(int argc, char const *argv[])
{
	try
	{
		menu();
	} catch (std::bad_alloc &e)
	{
		cout << " failed running game, bad allocation error, " << e.what()
				<< endl;
	} catch (const char *msg)
	{
		cout << " failed running game, reason: " << msg << endl;
	}
	return (0);
}

