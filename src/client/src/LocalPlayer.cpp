/*
 * LocalPlayer.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/LocalPlayer.h"
using namespace std;

LocalPlayer::LocalPlayer(const int &symbol) :
		Player(symbol), player_symbol_(empty), opponent_symbol_(empty), player_turn_(
				false)
{
	setPlayerSymbol(symbol);
	if (symbol == player_1)
	{
		setOpponentSymbol(player_2);
	}
	else if (symbol == player_2)
	{
		setOpponentSymbol(player_1);
	}
}
LocalPlayer::LocalPlayer(const Player &other_player) :
		Player(other_player.getPlayerSymbol()), player_symbol_(
				other_player.getPlayerSymbol()), opponent_symbol_(
				other_player.getOpponentSymbol()), player_turn_(
				other_player.isPlayerTurn())
{
}
void LocalPlayer::playOneMove(
		std::vector<std::vector<GamePiece> >& player_moves, Board &game_board)
{
	printPlayerMoves(player_moves);
	int move_index = getValidMoveIndex(player_moves);
	game_board.flipAllPiecesInRange(player_moves[move_index]);
	game_board.printBoard();
}
void LocalPlayer::printPlayerMoves(
		std::vector<std::vector<GamePiece> >& player_moves)
{
	cout << "Player " << ((char) getPlayerSymbol()) << ": it's your turn."
			<< endl;
	cout << "your possible moves are: ";
	std::size_t player_moves_size = player_moves.size();
	//print valid player moves
	for (std::size_t i = 0; i < player_moves_size; i++)
	{
		std::vector<GamePiece> v = player_moves[i];
		GamePiece g = v.back();
		cout << g.getPieceLocationPoint().pointToString();
		if (i < player_moves.size() - 1)
		{
			cout << ",";
		}
	}
	cout << endl;
}
Point LocalPlayer::getUserInput()
{
	int row = -1;
	int column = -1;
	int i = 1;
	//gets the user input for next player move.
	do
	{
		cout << "choose a move (row,column) to play: [ input example: point "
				"(1,2) will be: 1 2 ]" << endl;
		cin >> row >> column;
		if (!cin.fail())
		{
			i = 0;
		}
		else
		{
			cout << "Error: input not in format - only 2 numbers are "
					"allowed separated by space." << endl;
			row = column = -1;
		}
		cin.clear();
		cin.ignore(10000, '\n');
	} while (i == 1);
	return (Point(row, column));
}

int LocalPlayer::getValidMoveIndex(
		const std::vector<std::vector<GamePiece> >& player_moves)
{
	int end = 1;
	int move_index = -1;
	std::size_t player_moves_size = player_moves.size();
	do
	{
		//get the user input (next player move) and check if its a valid move.
		Point p = getUserInput();
		for (std::size_t i = 0; i < player_moves_size; i++)
		{
			Point p1 = player_moves[i].back().getPieceLocationPoint();
			if (p1 == p)
			{
				end = 0;
				move_index = i;
			}
		}
		if (end == 1)
		{
			cout << "Error: entered invalid move, try again." << endl;
		}
	} while (end == 1);
	return (move_index);
}
const int LocalPlayer::getPlayerSymbol() const
{
	return (player_symbol_);
}

void LocalPlayer::setPlayerSymbol(const int &player_symbol)
{
	player_symbol_ = player_symbol;
}

bool LocalPlayer::isPlayerTurn() const
{
	return (player_turn_);
}

void LocalPlayer::setPlayerTurn(const bool &player_turn)
{
	player_turn_ = player_turn;
}

const int LocalPlayer::getOpponentSymbol() const
{
	return (opponent_symbol_);
}

void LocalPlayer::setOpponentSymbol(const int &opponent_symbol)
{
	opponent_symbol_ = opponent_symbol;
}

LocalPlayer::~LocalPlayer()
{
}
