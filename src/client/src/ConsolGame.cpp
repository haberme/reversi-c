/*
 * ConsolGame.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/ConsolGame.h"
using namespace std;

ConsolGame::ConsolGame(const int &row, const int &column, Player *&p1,
		Player *&p2) :
		Game(row, column, p1, p2), row_size_(row), column_size_(column), rules_(
				new BasicGameRules()), game_board_(
				new Board(row, column, rules_)), player_1_(p1), player_2_(p2)
{
}
void ConsolGame::play(){
	getPlayer1()->setPlayerTurn(true);
	bool game_over = false;
	this->getGameBoard()->printBoard();
	const Counter * score_counter= getGameBoard()->getScoreCounter();

	//plays one turn until game over conditions are met.
	do
	{
		playOneTurn();
		game_over = getGameRules()->isGameOver(score_counter,
				getPlayer1()->isPlayerTurn(), getPlayer2()->isPlayerTurn());
	} while (!game_over);
	gameOver();
}
void ConsolGame::gameOver()
{
	cout << endl << endl << "********   Game Over   ********" << endl << endl;
	char winner_symbol = getGameRules()->getWinnerSymbol();
	int winner_score = getGameRules()->getWinnerScore();
	if ((winner_symbol == empty) && (winner_score == -10))
	{
		cout << "        It`s a Tie!! there are no winners!!!" << endl << endl;
	}
	else
	{
		cout << "        The Winner is Player : " << winner_symbol
				<< "  with a score of: " << winner_score << endl << endl;
	}
}

void ConsolGame::playOneTurn()
{
	Player* player;
	Player* opponent;
	Player* p1 = getPlayer1();
	Player* p2 = getPlayer2();
	if (p1->isPlayerTurn() && !p2->isPlayerTurn())
	{
		player = p1;
		opponent = p2;
	}
	if (!p1->isPlayerTurn() && p2->isPlayerTurn())
	{
		opponent = p1;
		player = p2;
	}
	int player_symbol = player->getPlayerSymbol();
	int opponent_symbol = player->getOpponentSymbol();
	//get the possible moves for player.
	std::vector<std::vector<GamePiece> > player_moves =
			getGameBoard()->getPlayerPossibleMoves(player_symbol,
					opponent_symbol);
	if (!player_moves.empty())
	{
		player->playOneMove(player_moves, *getGameBoard());
		opponent->setPlayerTurn(true);
	}
	else
	{
		cout << "no possible moves found for player " << (char) player_symbol
				<< endl;
		//if the player has no more moves check if his opponent has moves.
		std::vector<std::vector<GamePiece> > opponent_moves =
				getGameBoard()->getPlayerPossibleMoves(opponent_symbol,
						player_symbol);
		if (!opponent_moves.empty())
		{
			opponent->setPlayerTurn(true);
		}
		else
		{
			cout << "no possible moves found for player "
					<< (char) opponent_symbol << endl;
		}
	}
	player->setPlayerTurn(false);
}

GameRules*& ConsolGame::getGameRules()
{
	return (this->rules_);
}

const int ConsolGame::getColumnSize() const
{
	return (this->column_size_);
}

Board*& ConsolGame::getGameBoard()
{
	return (this->game_board_);
}

const int ConsolGame::getRowSize() const
{
	return (this->row_size_);
}

Player*& ConsolGame::getPlayer1()
{
	return (this->player_1_);
}

Player*& ConsolGame::getPlayer2()
{
	return (this->player_2_);
}

ConsolGame::~ConsolGame()
{
	delete game_board_;
	delete rules_;
}
