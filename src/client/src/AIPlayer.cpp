/*
 * AIPlayer.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/AIPlayer.h"
using namespace std;

AIPlayer::AIPlayer(const int &symbol) :
		LocalPlayer(symbol)
{
}
;
AIPlayer::AIPlayer(const Player &other_player) :
		LocalPlayer(other_player.getPlayerSymbol())
{
}
;

void AIPlayer::playOneMove(std::vector<std::vector<GamePiece> >& player_moves,
		Board &game_board)
{
	//the pair returned is : <best_move_index,best_move_score>
	pair<int, int> best_move = game_board.getPlayerMinMaxMove(player_moves);
	Point played_move =
			player_moves[best_move.first].back().getPieceLocationPoint();
	game_board.flipAllPiecesInRange(player_moves[best_move.first]);
	game_board.printBoard();
	cout << "Player " << (char) getPlayerSymbol() << " played move: "
			<< played_move.pointToString() << endl << endl;
}

AIPlayer::~AIPlayer()
{
}
;
