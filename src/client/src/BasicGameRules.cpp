/*
 * BasicGameRules.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */
#include "../include/BasicGameRules.h"
using namespace std;

BasicGameRules::BasicGameRules() :
		rule_description_(
				"Basic game rules: "
						"1)player can only do moves if there is a empty cell at the end of his strike direction"
						"2)can`t finish a player strike if it has the player pieces in it."
						"3)invalid move - if the piece is at the edge of board and isn`t empty."
						"4)invalid move - if got to empty cell without encountering opponent."), winner_symbol_(
				empty), winner_score_(-1)
{
}
BasicGameRules::BasicGameRules(std::string &rule_description) :
		GameRules(rule_description), winner_symbol_(empty), winner_score_(-1)
{
	this->rule_description_ = rule_description;
}

const std::string BasicGameRules::getRuleDescription()
{
	return (rule_description_);
}

bool BasicGameRules::checkValidMove(const char & current_piece,
		const char &player_symbol, bool &opponent_encounterd,
		const bool & is_Board_Edge_piece)
{
	bool val = true;
	//can`t finish a player strike if it has the player pieces in it.
	if (current_piece == player_symbol)
	{
		val = false;
	}
	// invalid move - if the piece is at the edge of board and isn`t empty.
	if (current_piece != empty && is_Board_Edge_piece)
	{
		val = false;
	}
	// got to empty cell without encountering opponent.
	if (!opponent_encounterd && (current_piece == empty))
	{
		val = false;
	}
	//valid move - encountered opponent and found empty cell.
	if (opponent_encounterd && (current_piece == empty))
	{
		val = true;
	}
	return (val);
}
void BasicGameRules::setWinnerSymbol(const int & player_symbol)
{
	this->winner_symbol_ = player_symbol;
}
const int BasicGameRules::getWinnerSymbol() const
{
	return (this->winner_symbol_);
}
void BasicGameRules::setWinnerScore(const int & player_score)
{
	this->winner_score_ = player_score;
}
const int BasicGameRules::getWinnerScore() const
{
	return (this->winner_score_);
}
bool BasicGameRules::isGameOver(const Counter*& score_counter,
		const bool &player1_turn, const bool &player2_turn)
{
	bool val = false;
	int empty_cell = score_counter->getEmptyCells();
	int score_1 = score_counter->getPlayer1Score();
	int score_2 = score_counter->getPlayer2Score();
//none of the players turn - no more moves in game.
	if (!player1_turn && !player2_turn)
	{
		val = true;
	}
//no more empty cells - board is full.
	if (empty_cell == 0)
	{
		val = true;
	}
	if (score_1 > score_2)
	{
		setWinnerSymbol(player_1);
		setWinnerScore(score_1);
	}
	if (score_2 > score_1)
	{
		setWinnerSymbol(player_2);
		setWinnerScore(score_2);
	}
	if (score_1 == score_2)
	{
		setWinnerSymbol(empty);
		setWinnerScore(-10);
	}
	return (val);
}
BasicGameRules::~BasicGameRules()
{
}
