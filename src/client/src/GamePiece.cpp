/*
 * GamePiece.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */
#include "../include/GamePiece.h"
using namespace std;

GamePiece::GamePiece() :
		piece_row_(-1), piece_column_(-1), piece_location_point_(Point(-1, -1)), piece_symbol_(
				empty)
{
}

GamePiece::GamePiece(const int &row, const int &column, const int &symbol) :
		piece_row_(row), piece_column_(column), piece_location_point_(
				Point(row, column)), piece_symbol_(symbol)
{

}

GamePiece::GamePiece(const Point &location_p, const int &symbol) :
		piece_row_(location_p.getRow()), piece_column_(location_p.getColumn()), piece_location_point_(
				Point(location_p)), piece_symbol_(symbol)
{
	GamePiece(location_p.getRow(), location_p.getColumn(), symbol);
}
GamePiece::GamePiece(const GamePiece &other_game_piece) :
		piece_row_(other_game_piece.getGamePieceRow()), piece_column_(
				other_game_piece.getGamePieceColumn()), piece_location_point_(
				Point(other_game_piece.getPieceLocationPoint())), piece_symbol_(
				other_game_piece.getGamePieceSymbol())
{

}
int GamePiece::getGamePieceRow() const
{
	return (piece_row_);
}

void GamePiece::setGamePieceRow(const int &row)
{
	this->piece_row_ = row;
}

int GamePiece::getGamePieceColumn() const
{
	return (piece_column_);
}

void GamePiece::setGamePieceColumn(const int &column)
{
	this->piece_column_ = column;
}

Point GamePiece::getPieceLocationPoint() const
{
	return (this->piece_location_point_);
}

void GamePiece::setPieceLocationPoint(const Point &piece_location_point)
{
	this->piece_location_point_ = piece_location_point;
	setGamePieceRow(piece_location_point.getRow());
	setGamePieceColumn(piece_location_point.getColumn());
}

int GamePiece::getGamePieceSymbol() const
{
	return (piece_symbol_);
}

void GamePiece::setGamePieceSymbol(const int &symbol)
{
	this->piece_symbol_ = symbol;
}

const std::string GamePiece::gamePieceToString()
{
	std::string sp = getPieceLocationPoint().pointToString();
	std::string s = "game piece at point: " + sp + "  is : "
			+ (char) getGamePieceSymbol();
	return (s);
}
GamePiece::~GamePiece()
{
}
