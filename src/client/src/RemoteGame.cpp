/*
 * RemoteGame.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/RemoteGame.h"
using namespace std;
RemoteGame::RemoteGame(const int &row, const int &column, Player *&p1,
		Player *&p2) :
		ConsolGame(row, column, p1, p2)
{
}
void RemoteGame::play()
{
	RemotePlayer* remote_player = (RemotePlayer*) getPlayer1();
	int turn_number = 0;
	//if player1 is the second player ( O ) then we invoke a method to read player X first move.
	if (getPlayer1()->getPlayerSymbol() == player_2)
	{
		getGameBoard()->printBoard();
		remote_player->readRemoteOpponentMove(turn_number, false);
		getPlayer2()->setPlayerTurn(true);
	}
	else
	{
		getPlayer1()->setPlayerTurn(true);
	}
	bool is_game_over = false;
	Point remote_opponent_move = remote_player->getRemoteOpponentMove();
	//plays one turn until game over conditions are met.
	do
	{
		turn_number++;
		remote_opponent_move = remote_player->getRemoteOpponentMove();
		//break out of loop if got signal of game over from remote opponent.
		if (remote_opponent_move == Point(game_over, game_over))
		{
			break;
		}
		playOneTurn(turn_number);
		is_game_over = checkGameOver();
	} while (!is_game_over);
	remote_opponent_move = remote_player->getRemoteOpponentMove();
	if (remote_opponent_move != Point(game_over, game_over))
	{
		remote_player->sendGameOverSignal();
	}
	gameOver();
}

void RemoteGame::playOneGivenMove(Point& move_to_play,
		std::vector<std::vector<GamePiece> >& possible_moves)
{
	// plays the move at the given point move_to_play and prints board.
	std::vector<GamePiece> b = getGameBoard()->getStrikeVector(possible_moves,
			move_to_play);
	if (!b.empty())
	{
		getGameBoard()->flipAllPiecesInRange(b);
		getGameBoard()->printBoard();
		if (move_to_play != Point(no_move, no_move))
		{
			cout << "Player " << (char) getPlayer2()->getPlayerSymbol()
					<< " played move: " << move_to_play.pointToString() << endl
					<< endl;
		}
	}
}

bool RemoteGame::checkGameOver()
{
	bool is_game_over = false;
	const Counter * score_counter = getGameBoard()->getScoreCounter();
	is_game_over = getGameRules()->isGameOver(score_counter,
			getPlayer1()->isPlayerTurn(), getPlayer2()->isPlayerTurn());
	int p1_symbol = getPlayer1()->getPlayerSymbol();
	int p2_symbol = getPlayer2()->getPlayerSymbol();
	std::vector<std::vector<GamePiece> > p1_moves =
			getGameBoard()->getPlayerPossibleMoves(p1_symbol, p2_symbol);
	std::vector<std::vector<GamePiece> > p2_moves =
			getGameBoard()->getPlayerPossibleMoves(p2_symbol, p1_symbol);
	//checks if both player have moves
	if (p1_moves.empty() && p2_moves.empty())
	{
		if (getPlayer1()->isPlayerTurn() && !getPlayer2()->isPlayerTurn())
		{
			cout << "no possible moves found for player " << (char) p1_symbol
					<< endl;
			cout << "no possible moves found for player " << (char) p2_symbol
					<< endl;
		}
		//both moves are empty game over.
		return (true);
	}
	return (is_game_over);
}
void RemoteGame::playOneTurn(int& turn_number)
{
	Player* player;
	Player* opponent;
	Player* p1 = getPlayer1();
	Player* p2 = getPlayer2();
	RemotePlayer* remote_player = (RemotePlayer*) p1;
	if (p1->isPlayerTurn() && !p2->isPlayerTurn())
	{
		player = p1;
		opponent = p2;
	}
	if (!p1->isPlayerTurn() && p2->isPlayerTurn())
	{
		opponent = p1;
		player = p2;
	}
	int player_symbol = player->getPlayerSymbol();
	int opponent_symbol = player->getOpponentSymbol();
	//get the possible moves for player.
	std::vector<std::vector<GamePiece> > player_moves =
			getGameBoard()->getPlayerPossibleMoves(player_symbol,
					opponent_symbol);
	if (!player_moves.empty())
	{
		if (p1->isPlayerTurn())
		{
			remote_player->playOneMove(player_moves, *getGameBoard(),
					turn_number);
			remote_player->readRemoteOpponentMove(turn_number, checkGameOver());
		}
		else
		{
			// its player 2 turn, we play remote opponent move.
			Point move_to_play = remote_player->getRemoteOpponentMove();
			if (move_to_play != Point(no_move, no_move))
			{
				playOneGivenMove(move_to_play, player_moves);
			}
		}
		opponent->setPlayerTurn(true);
	}
	else
	{
		cout << "no possible moves found for player " << (char) player_symbol
				<< endl;
		if (p1->isPlayerTurn())
		{
			// if player 1 have no move , write to server no move and read opponent move.
			remote_player->writeNoMove();
			remote_player->readRemoteOpponentMove(turn_number, checkGameOver());
		}
		//if the player has no more moves check if his opponent has moves.
		std::vector<std::vector<GamePiece> > opponent_moves =
				getGameBoard()->getPlayerPossibleMoves(opponent_symbol,
						player_symbol);
		if (!opponent_moves.empty())
		{
			opponent->setPlayerTurn(true);
		}
		else
		{
			cout << "no possible moves found for player "
					<< (char) opponent_symbol << endl;
		}
	}
	player->setPlayerTurn(false);
}
RemoteGame::~RemoteGame()
{
}

