/*
 * RemotePlayer.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/RemotePlayer.h"
using namespace std;
RemotePlayer::RemotePlayer(const char * &server_IP, const int &server_port,
		Player*& opponent) :
		LocalPlayer(empty), server_IP_(server_IP), server_port_(server_port), client_socket_(
				0), opponent_move_(Point(no_move, no_move))
{
	try
	{
		connectToServer(opponent);
	} catch (const char* msg)
	{
		cout<<msg<<endl;
		throw "in RemotePlayer::RemotePlayer failed connecting to server ";
		return;
	}
}

void RemotePlayer::connectToServer(Player*& opponent)
{
	// Create a client socket point
	setClientSocket(socket(AF_INET, SOCK_STREAM, 0));
	if (getClientSocket() == -1)
	{
		throw "Error: in RemotePlayer::connectToServer failed opening client socket";
	}
	// Convert the ip string to a network address
	struct in_addr address;
	if (!inet_aton(getServerIp(), &address))
	{
		throw "Error: in RemotePlayer::connectToServer Can't parse server IP address";
	}
	// Get a hostent structure for the given host address
	struct hostent *server;
	server = gethostbyaddr((const void *) &address, sizeof(address), AF_INET);
	if (server == NULL)
	{
		throw "Error: in RemotePlayer::connectToServer server Host is unreachable";
	}
	// Create a structure for the client address
	struct sockaddr_in serverAddress;
	memset((char *) &serverAddress, 0, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	memcpy((char *) &serverAddress.sin_addr.s_addr, (char*) server->h_addr, server->h_length);
	// htons converts values between host and network byte orders
	serverAddress.sin_port = htons(getServerPort());
	// Establish a connection with the TCP server
	if (connect(getClientSocket(), (struct sockaddr *) &serverAddress,
			sizeof(serverAddress)) == -1)
	{
		throw "Error: in RemotePlayer::connectToServer failed connecting to server";
	}
	else
	{
		cout << "Connected to server." << endl;
		/* wait for server to send the player number/symbol (1='X',2='O') */
		int symbol = readFromServer();
		if ((symbol != client_1) && (symbol != client_2))
		{
			throw "Error: in RemotePlayer::connectToServer failed creating player,wrong player number received from server.";
		}
		if (symbol == client_1)
		{
			cout << "waiting for other player to join.." << endl;
			setPlayerSymbol(player_1);
			setOpponentSymbol(player_2);
			((LocalPlayer*) opponent)->setOpponentSymbol(player_1);
			((LocalPlayer*) opponent)->setPlayerSymbol(player_2);

		}
		if (symbol == client_2)
		{
			cout << endl;
			setPlayerSymbol(player_2);
			setOpponentSymbol(player_1);
			((LocalPlayer*) opponent)->setOpponentSymbol(player_2);
			((LocalPlayer*) opponent)->setPlayerSymbol(player_1);
		}
		cout << endl << endl;
	}
}

void RemotePlayer::sendGameOverSignal()
{
	try
	{
		int write = writeToServer(game_over);
		write = writeToServer(game_over);
		setGameOverMove();
	} catch (const char* msg)
	{
		throw "Error in RemotePlayer::sendGameOverSignal, failed sending game over signal to server ";
	}
}

void RemotePlayer::readRemoteOpponentMove(int& turn_number, bool is_game_over)
{
	if (!is_game_over)
	{
		cout << "waiting for player " << (char) getOpponentSymbol() << " move.."
				<< endl << endl;
	}
	/* get opponent move back from server, play that move*/
	int opponent_move_row = readFromServer();
	int opponent_move_column = readFromServer();
	Point opponent_move = Point(opponent_move_row, opponent_move_column);
	setRemoteOpponentMove(opponent_move);
	//opponent move invalid
	if ((opponent_move_row == error) || (opponent_move_column == error))
	{
		setGameOverMove();
		throw "Error: in RemotePlayer::readRemoteOpponentMove , opponent encountered some error.";
	}
	if ((opponent_move_row == disconnected)
			|| (opponent_move_column == disconnected))
	{
		setGameOverMove();
		throw "Error: in RemotePlayer::readRemoteOpponentMove , opponent has disconnected.";
	}
	if ((opponent_move_row == game_over) || (opponent_move_column == game_over))
	{
		setGameOverMove();
	}
}

void RemotePlayer::setGameOverMove()
{
	Point p = Point(game_over, game_over);
	setRemoteOpponentMove(p);
}

void RemotePlayer::writeNoMove()
{
	int write_row;
	int write_column;
	try
	{
		write_row = writeToServer(no_move);
		write_column = writeToServer(no_move);
	} catch (const char* msg)
	{
		throw "in RemotePlayer::writeNoMove failed writing no move to server";
	}
}

const int RemotePlayer::readFromServer()
{
	int read_result;
	/* read from client socket (the data the server sent) */
	int n = read(getClientSocket(), &read_result, sizeof(read_result));
	if (n == -1)
	{
		setGameOverMove();
		throw "Error: in RemotePlayer::readFromServer failed reading from socket";
		return (error);
	}
	if (n == 0)
	{
		setGameOverMove();
		throw "Error: in RemotePlayer::readFromServer socket is disconnected";
		return (disconnected);
	}
	if (read_result == disconnected)
	{
		setGameOverMove();
		throw "Error: in RemotePlayer::readFromServer other client is disconnected";
	}
	if (read_result == game_over)
	{
		setGameOverMove();
		return (game_over);
	}
	return (read_result);
}

const int RemotePlayer::writeToServer(const int &data_to_write)
{
	/* write to the given client socket the given data. */
	int n = write(getClientSocket(), &data_to_write, sizeof(data_to_write));
	if (n == -1)
	{
		throw "Error: in RemotePlayer::writeToServer failed writing to socket";
		setGameOverMove();
		return (error);
	}
	if (n == 0)
	{
		throw "Error: in RemotePlayer::writeToServer socket is disconnected";
		setGameOverMove();
		return (disconnected);
	}
	return (success);
}

void RemotePlayer::playOneMove(
		std::vector<std::vector<GamePiece> >& player_moves, Board &game_board,
		int& turn_number)
{
	int write_row;
	int write_column;
	int read_turn;
	//print the board for the first time if its player X.
	if ((turn_number == 1) && (getPlayerSymbol() == player_1))
	{
		try
		{
			read_turn = readFromServer();
		} catch (const char* msg)
		{
			throw "in RemotePlayer::playOneMove failed reading turn from server ";
			return;
		}
		game_board.printBoard();
	}
	printPlayerMoves(player_moves);
	int move_index = getValidMoveIndex(player_moves);
	Point player_move = player_moves[move_index].back().getPieceLocationPoint();
	/* play the move on the board.*/
	game_board.flipAllPiecesInRange(player_moves[move_index]);
	game_board.printBoard();
	try
	{
		/* send the move played to the server .*/
		write_row = writeToServer(player_move.getRow());
		write_column = writeToServer(player_move.getColumn());
	} catch (const char* msg)
	{
		throw "in RemotePlayer::playOneMove failed writing move to server";
	}
}

Point & RemotePlayer::getRemoteOpponentMove()
{
	return (opponent_move_);
}

void RemotePlayer::setRemoteOpponentMove(Point& opponentMove)
{
	opponent_move_ = opponentMove;
}

int RemotePlayer::getClientSocket() const
{
	return (client_socket_);
}

void RemotePlayer::setClientSocket(const int& clientSocket)
{
	client_socket_ = clientSocket;
}

const char* RemotePlayer::getServerIp() const
{
	return (server_IP_);
}

void RemotePlayer::setServerIp(const char* serverIp)
{
	server_IP_ = serverIp;
}

int RemotePlayer::getServerPort() const
{
	return (server_port_);
}

void RemotePlayer::setServerPort(const int& serverPort)
{
	server_port_ = serverPort;
}
RemotePlayer::~RemotePlayer()
{
}

