/*
 * Board.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/Board.h"
using namespace std;

enum Direction
{
	NORTH = 0,
	NORTH_EAST = 45,
	EAST = 90,
	SOUTH_EAST = 135,
	SOUTH = 180,
	SOUTH_WEST = 225,
	WEST = 270,
	NORTH_WEST = 315
};
Board::Board(const int &row_s, const int &column_s, GameRules *& game_rules) :
		row_size_(row_s), column_size_(column_s), score_counter_(new Counter()), game_rules_(
				game_rules)
{
	initializeBoard();
}
Board::Board(const Board& other) :
		row_size_(other.getRowSize()), column_size_(other.getColumnSize()), score_counter_(
				new Counter(*other.score_counter_)), game_matrix_(
				other.game_matrix_), game_rules_(other.game_rules_)
{
	int row_s = other.getRowSize();
	int column_s = other.getColumnSize();
	try
	{
		GamePiece** other_matrix = other.game_matrix_;
		GamePiece** temp_matrix = new GamePiece*[row_s];
		for (int m = 0; m < row_s; m++)
		{
			temp_matrix[m] = new GamePiece[column_s];
			for (int n = 0; n < column_s; n++)
			{
				temp_matrix[m][n] = other_matrix[m][n];
			}
		}
		setGameMatrix(temp_matrix);
	} catch (std::bad_alloc& e)
	{
		cout
				<< "Error: failed to create game board matrix in Board:: copy constructor."
				<< endl;
		return;
	}

}
void Board::initializeBoard()
{
	int row_s = getRowSize();
	int column_s = getColumnSize();
	int empty_cells = 0;
	try
	{
		GamePiece** temp_matrix = new GamePiece*[row_s];
		for (int m = 0; m < row_s; m++)
		{
			temp_matrix[m] = new GamePiece[column_s];
			for (int n = 0; n < column_s; n++)
			{
				temp_matrix[m][n].setPieceLocationPoint(Point(m, n));
				empty_cells++;
			}
		}
		getScoreCounter()->setEmptyCells(empty_cells);
		setGameMatrix(temp_matrix);
	} catch (std::bad_alloc &e)
	{
		cout
				<< "Error: failed to create game board matrix in Board::initializeBoard()."
				<< endl;
		return;
	}
	//initialize the first 4 game pieces.
	int x = row_s / 2;
	int y = column_s / 2;
	addGamePiece(x, y - 1, player_1);
	addGamePiece(x - 1, y, player_1);
	addGamePiece(x - 1, y - 1, player_2);
	addGamePiece(x, y, player_2);
}
void Board::printBoardLines()
{
	int column_s = getColumnSize();
	std::string s5 = "-----";
	std::string s3 = "---";
	cout << "-";
	// Dynamically change the number of separating lines based on number of columns.
	for (int i = 0; i < column_s; i++)
	{
		if (i % 2 == 0)
		{
			cout << s5;
		}
		else
		{
			cout << s3;
		}
	}
	if (column_s % 2 == 0)
	{
		cout << "-" << endl;
	}
	else
	{
		cout << endl;
	}
}
void Board::printBoard()
{
	GamePiece** matrix = getGameMatrix();
	int row_s = getRowSize();
	int column_s = getColumnSize();
	cout << "current board:" << endl << endl;
	//print the columns numbers
	for (int i = 0; i < column_s; i++)
	{
		cout << " | " << i;
	}
	cout << " |" << endl;
	//print each row of the matrix
	for (int m = 0; m < row_s; m++)
	{
		printBoardLines();
		cout << m << "|";
		for (int n = 0; n < column_s; n++)
		{
			char current_symbol = matrix[m][n].getGamePieceSymbol();
			cout << " " << current_symbol << " |";
		}
		cout << endl;
	}
	printBoardLines();
	cout << "Current game scores:   | Player X: "
			<< getScoreCounter()->getPlayer1Score() << " | Player O: "
			<< getScoreCounter()->getPlayer2Score() << " | empty cells: "
			<< getScoreCounter()->getEmptyCells() << " |" << endl << endl;
}
std::vector<GamePiece> Board::getStrikeVector(
		std::vector<std::vector<GamePiece> >& possible_moves,
		Point point_to_play)
{
	std::size_t possible_moves_size = possible_moves.size();
	std::vector<GamePiece> strike_vector;
	for (std::size_t i = 0; i < possible_moves_size; i++)
	{
		if (possible_moves[i].back().getPieceLocationPoint() == point_to_play)
		{
			strike_vector = possible_moves[i];
		}
	}
	return (strike_vector);
}

Board* Board::createBoardWithMoves(
		std::vector<std::pair<Point, int> > & play_moves, Board *&other_board)
{
	Board* current_board = new Board(*other_board);
	int player;
	int opponent;
	int move_index;
	std::size_t play_moves_size = play_moves.size();
	std::vector<std::vector<GamePiece> > player_moves;
	for (std::size_t i = 0; i < play_moves_size; i++)
	{
		Point move = play_moves[i].first;
		player = play_moves[i].second;
		if (player == player_1)
		{
			opponent = player_2;
		}
		if (player == player_2)
		{
			opponent = player_1;
		}
		if ((player != player_1) && (player != player_2))
		{
			break;
		}
		//get the player moves for the current board.
		player_moves = current_board->getPlayerPossibleMoves(player, opponent);
		if (!player_moves.empty())
		{
			move_index = current_board->getMoveIndex(player_moves, move);
			// if move_index= -1 , means the move was not found,we wont play the move.
			if (move_index > -1)
			{
				current_board->flipAllPiecesInRange(player_moves[move_index]);
			}
		}
	}
	return (current_board);
}
int Board::getMoveIndex(
		const std::vector<std::vector<GamePiece> > &player_moves,
		const Point &move)
{
	int move_index = -1;
	std::size_t player_moves_size = player_moves.size();
	if (player_moves_size > 0)
	{
		for (std::size_t i = 0; i < player_moves_size; ++i)
		{
			Point player_move = player_moves[i].back().getPieceLocationPoint();
			if (player_move == move)
			{
				move_index = i;
			}
		}
	}
	return (move_index);
}

void Board::printVector(std::vector<GamePiece> vector)
{
	std::size_t vector_size = vector.size();
	if (vector_size > 0)
	{
		cout << "************ Printing vector<gamePiece> ************" << endl;
		cout << "vector size is: " << vector.size() << endl;
		for (std::size_t i = 0; i < vector_size; i++)
		{
			GamePiece g = vector[i];
			cout << "i is: " << i << " point: "
					<< g.getPieceLocationPoint().pointToString() << endl;
		}
	}
	cout << "********************************************************" << endl;

}
bool Board::isBoardEdgePiece(const GamePiece &piece, const int &direction)
{
	int board_row_s = getRowSize();
	int board_column_s = getColumnSize();
	int piece_row = piece.getGamePieceRow();
	int piece_column = piece.getGamePieceColumn();
	bool val = false;
	switch (direction)
	{
		case NORTH:
			if (piece_row == 0)
			{
				val = true;
			}
			break;
		case NORTH_EAST:
			if ((piece_row == 0) && (piece_column == board_column_s - 1))
			{
				val = true;
			}
			break;
		case EAST:
			if (piece_column == board_column_s - 1)
			{
				val = true;
			}
			break;
		case SOUTH_EAST:
			if ((piece_row == board_row_s - 1)
					&& (piece_column == board_column_s - 1))
			{
				val = true;
			}
			break;
		case SOUTH:
			if (piece_row == board_row_s - 1)
			{
				val = true;
			}
			break;
		case SOUTH_WEST:
			if ((piece_row == board_row_s - 1) && (piece_column == 0))
			{
				val = true;
			}
			break;
		case WEST:
			if (piece_column == 0)
			{
				val = true;
			}
			break;
		case NORTH_WEST:
			if ((piece_row == 0) && (piece_column == 0))
			{
				val = true;
			}
			break;
		default:
			cout << "at direction switch default case" << endl;
			break;
	}
	return (val);
}

void Board::checkMovesInDirection(const int &row, const int &column,
		const int &direction, const int &opponent,
		std::vector<std::vector<GamePiece> >& possible_moves)
{
	GamePiece** matrix = getGameMatrix();
	GameRules * rules = getGameRules();
	std::vector<GamePiece> strike_vector;
	int board_row_s = getRowSize();
	int board_column_s = getColumnSize();
	int i = 1;
	int end = 1;
	int m = row;
	int n = column;
	bool valid_move = false;
	bool opponent_encounterd = false;
	char player_symbol = matrix[m][n].getGamePieceSymbol();
	char current_piece;
	do
	{
		//add the first game piece at the strike vector (strike origin piece).
		if (i == 1)
		{
			strike_vector.push_back(matrix[m][n]);
		}
		//direction to check
		switch (direction)
		{
			case NORTH:
				m = row - i;
				n = column;
				break;
			case NORTH_EAST:
				m = row - i;
				n = column + i;
				break;
			case EAST:
				m = row;
				n = column + i;
				break;
			case SOUTH_EAST:
				m = row + i;
				n = column + i;
				break;
			case SOUTH:
				m = row + i;
				n = column;
				break;
			case SOUTH_WEST:
				m = row + i;
				n = column - i;
				break;
			case WEST:
				m = row;
				n = column - i;
				break;
			case NORTH_WEST:
				m = row - i;
				n = column - i;
				break;
			default:
				cout << "at direction switch default case" << endl;
				break;
		}
		//out of bounds.
		if ((n < 0) || (n >= board_column_s) || (m < 0) || (m >= board_row_s))
		{
			strike_vector.clear();
			break;
		}
		current_piece = matrix[m][n].getGamePieceSymbol();
		if (current_piece == opponent)
		{
			opponent_encounterd = true;
		}
		valid_move = rules->checkValidMove(current_piece, player_symbol,
				opponent_encounterd, isBoardEdgePiece(matrix[m][n], direction));
		if (!valid_move)
		{
			strike_vector.clear();
			break;
		}
		if (valid_move)
		{
			strike_vector.push_back(matrix[m][n]);
			//no valid strike vector for less then 3 game pieces.
			if ((current_piece == empty) && (strike_vector.size() > 2))
			{
				if (possible_moves.size() > 0)
				{
					/* checks if we have a strike vector for the same move,
					 * a strike_index of -1 means we don`t have vectors that end
					 * in the same point(move). */
					int strike_index = this->getMoveIndex(possible_moves,
							strike_vector.back().getPieceLocationPoint());
					//merge the vectors if duplicate found.
					if (strike_index > -1)
					{
						std::size_t strike_vector_size = strike_vector.size();
						for (std::size_t j = 0; j < strike_vector_size; j++)
						{
							possible_moves[strike_index].push_back(
									strike_vector[j]);
						}
					}
					else
					{
						//no duplicate strike vector found, add the strike vector.
						possible_moves.push_back(strike_vector);
					}
				}
				else
				{
					//possible moves is empty, add the strike vector.
					possible_moves.push_back(strike_vector);
				}
				break;
			}
		}
		i++;
	} while (end == 1);
}

std::vector<std::vector<GamePiece> > Board::getPlayerPossibleMoves(
		const int &player_symbol, const int &opponent_symbol)
{
	GamePiece** matrix = getGameMatrix();
	int row_s = getRowSize();
	int column_s = getColumnSize();
	std::vector<std::vector<GamePiece> > possible_moves;
	//check the whole game board for player valid moves.
	for (int m = 0; m < row_s; m++)
	{
		for (int n = 0; n < column_s; n++)
		{
			int current_piece = matrix[m][n].getGamePieceSymbol();
			//player game piece found,checks all directions for valid moves.
			if (current_piece == player_symbol)
			{
				checkMovesInDirection(m, n, NORTH, opponent_symbol,
						possible_moves);
				checkMovesInDirection(m, n, NORTH_EAST, opponent_symbol,
						possible_moves);
				checkMovesInDirection(m, n, EAST, opponent_symbol,
						possible_moves);
				checkMovesInDirection(m, n, SOUTH_EAST, opponent_symbol,
						possible_moves);
				checkMovesInDirection(m, n, SOUTH, opponent_symbol,
						possible_moves);
				checkMovesInDirection(m, n, SOUTH_WEST, opponent_symbol,
						possible_moves);
				checkMovesInDirection(m, n, WEST, opponent_symbol,
						possible_moves);
				checkMovesInDirection(m, n, NORTH_WEST, opponent_symbol,
						possible_moves);
			}
		}
	}
	return (possible_moves);
}

void Board::flipAllPiecesInRange(std::vector<GamePiece> & strike_vector)
{
	if (!strike_vector.empty())
	{
		//the player symbol to change all other game pieces in the strike path.
		int flipTo = strike_vector[0].getGamePieceSymbol();
		std::size_t strike_vector_size = strike_vector.size();
		for (std::size_t i = 0; i < strike_vector_size; i++)
		{
			addGamePiece(strike_vector[i].getGamePieceRow(),
					strike_vector[i].getGamePieceColumn(), flipTo);
		}
	}else{
		cout<<"empty vector"<<endl;
	}
}
int Board::getColumnSize() const
{
	return (column_size_);
}

void Board::setColumnSize(const int &columnSize)
{
	column_size_ = columnSize;
}

int Board::getRowSize() const
{
	return (row_size_);
}

void Board::setRowSize(const int &rowSize)
{
	row_size_ = rowSize;
}

GamePiece * *Board::getGameMatrix()
{
	return (game_matrix_);
}

void Board::setGameMatrix(GamePiece** &gameMatrix)
{
	game_matrix_ = gameMatrix;
}
pair<int, int> Board::getPlayerMinMaxMove(
		std::vector<std::vector<GamePiece> > &possible_moves)
{
	std::size_t possible_moves_size = possible_moves.size();
	pair<int, int> player_best_move = std::make_pair(0, 0);
	if (possible_moves_size > 0)
	{
		std::vector<std::vector<GamePiece> > opponent_possible_moves;
		/*each strike vector starts at the player piece and ends at the player chosen move. */
		const int player_symbol = possible_moves[0][0].getGamePieceSymbol();
		int opponent_symbol;
		//the point row is the move index and the column is the score.
		pair<int, int> opponent_worst_move = std::make_pair(0, 100);
		pair<int, int> opponent_current_best_move = std::make_pair(0, 0);
		if (player_symbol == player_1)
		{
			opponent_symbol = player_2;
		}
		if (player_symbol == player_2)
		{
			opponent_symbol = player_1;
		}
		for (std::size_t i = 0; i < possible_moves_size; i++)
		{
			//create a copy of this current board and play the move in index i.
			Board* temp_board = new Board(*this);
			temp_board->flipAllPiecesInRange(possible_moves[i]);
			//based on the i`th new move performed , get the opponent possible moves.
			opponent_possible_moves = temp_board->getPlayerPossibleMoves(
					opponent_symbol, player_symbol);
			if (!opponent_possible_moves.empty())
			{
				//get the best move for the opponent.
				opponent_current_best_move = temp_board->getPlayerBestMoveIndex(
						opponent_possible_moves);
				/*the pair returned is : <best_move_index,best_move_score>
				 * therefore we keep the move point with the lowest score. */
				if (opponent_current_best_move.second
						< opponent_worst_move.second)
				{
					opponent_worst_move = opponent_current_best_move;
					player_best_move = std::make_pair(i,
							temp_board->getBoardScore(player_symbol));
				}
			}
			delete temp_board;
		}
	}
	//the pair returned is : <best_move_index,best_move_score>
	return (player_best_move);
}
int Board::checkMoveScore(std::vector<GamePiece>& strike_vector)
{
	int board_score = 0;
	if (!strike_vector.empty())
	{
		//create a copy of this current board and play the move in index i.
		Board* temp_board = new Board(*this);
		//Perform the strike_vector move.
		temp_board->flipAllPiecesInRange(strike_vector);
		//get the temp_board score in favor of player based on the move done.
		board_score = temp_board->getBoardScore(
				strike_vector[0].getGamePieceSymbol());
		delete temp_board;
	}
	return (board_score);
}
pair<int, int> Board::getPlayerBestMoveIndex(
		std::vector<std::vector<GamePiece> > &possible_moves)
{
	std::size_t possible_moves_size = possible_moves.size();
	int max_score = 0;
	int best_move_index = 0;
	if (possible_moves_size > 0)
	{
		max_score = checkMoveScore(possible_moves[0]);
		int current_score;
		//get the move with the highest score among the possible moves vector.
		for (std::size_t i = 1; i < possible_moves_size; i++)
		{
			current_score = checkMoveScore(possible_moves[i]);
			if (current_score > max_score)
			{
				max_score = current_score;
				best_move_index = i;
			}
		}
	}
	return (std::make_pair(best_move_index, max_score));
}
pair<int, int> Board::getPlayerWorstMoveIndex(
		std::vector<std::vector<GamePiece> > &possible_moves)
{
	std::size_t possible_moves_size = possible_moves.size();
	int min_score = 0;
	int worst_move_index = 0;
	if (possible_moves_size > 0)
	{
		min_score = checkMoveScore(possible_moves[0]);
		int current_score;
		//get the move with the lowest score among the possible moves vector.
		for (std::size_t i = 1; i < possible_moves_size; i++)
		{
			current_score = checkMoveScore(possible_moves[i]);
			if (current_score < min_score)
			{
				min_score = current_score;
				worst_move_index = i;
			}
		}
	}
	return (std::make_pair(worst_move_index, min_score));
}
int Board::getBoardScore(const int &player)
{
	//returns the board score based on the player, will return player-opponent.
	return (this->getScoreCounter()->getBoardScore(player));
}
void Board::addGamePiece(const int &row, const int &column, const int &symbol)
{
	//changes the symbol of the gamePiece at point (row,column).
	GamePiece** matrix = getGameMatrix();
	const int symbol_before = matrix[row][column].getGamePieceSymbol();
	matrix[row][column].setGamePieceSymbol(symbol);
	//update the game board score counter.
	getScoreCounter()->addToPlayer(symbol_before, symbol);
}

Counter * &Board::getScoreCounter()
{
	return (score_counter_);
}
GameRules * &Board::getGameRules()
{
	return (this->game_rules_);
}

bool Board::operator ==(Board& other) const
{
	//not the same board size ( row, column).
	if ((this->getColumnSize() != other.getColumnSize())
			|| (this->getRowSize() != other.getRowSize()))
	{
		return (false);
	}

	Counter* this_counter = this->score_counter_;
	Counter* other_counter = other.score_counter_;
	//not the same counter scores
	if ((this_counter->getEmptyCells() != other_counter->getEmptyCells())
			|| (this_counter->getPlayer1Score()
					!= other_counter->getPlayer1Score())
			|| (this_counter->getPlayer2Score()
					!= other_counter->getPlayer2Score()))
	{
		return (false);
	}
	//check that both game matrix are the same.
	GamePiece** this_matrix = this->game_matrix_;
	GamePiece** other_matrix = other.getGameMatrix();
	int row_s = getRowSize();
	int column_s = getColumnSize();
	for (int m = 0; m < row_s; m++)
	{
		for (int n = 0; n < column_s; n++)
		{
			if (this_matrix[m][n].getGamePieceSymbol()
					!= other_matrix[m][n].getGamePieceSymbol())
			{
				return (false);
			}
		}
	}
	return (true);
}

bool Board::operator !=(Board& other) const
{
	//not the same board size ( row, column).
	if ((this->getColumnSize() != other.getColumnSize())
			|| (this->getRowSize() != other.getRowSize()))
	{
		return (true);
	}

	Counter* this_counter = this->score_counter_;
	Counter* other_counter = other.score_counter_;
	//not the same counter scores
	if ((this_counter->getEmptyCells() != other_counter->getEmptyCells())
			|| (this_counter->getPlayer1Score()
					!= other_counter->getPlayer1Score())
			|| (this_counter->getPlayer2Score()
					!= other_counter->getPlayer2Score()))
	{
		return (true);
	}
	//check that both game matrix are the same.
	GamePiece** this_matrix = this->game_matrix_;
	GamePiece** other_matrix = other.getGameMatrix();
	int row_s = getRowSize();
	int column_s = getColumnSize();
	for (int m = 0; m < row_s; m++)
	{
		for (int n = 0; n < column_s; n++)
		{
			if (this_matrix[m][n].getGamePieceSymbol()
					!= other_matrix[m][n].getGamePieceSymbol())
			{
				return (true);
			}
		}
	}
	return (false);
}
Board::~Board()
{
	GamePiece** matrix = getGameMatrix();
	int row_s = getRowSize();
	if (matrix)
	{
		for (int i = row_s - 1; i >= 0; i--)
		{
			delete[] matrix[i];
		}
	}
	delete[] matrix;
	delete score_counter_;
}

