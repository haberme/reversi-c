/*
 * Point.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/Point.h"
using namespace std;

Point::Point(const int &row, const int &column) :
		row_(row), column_(column)
{
}
Point::Point(const Point &other_point) :
		row_(other_point.getRow()), column_(other_point.getColumn())
{
}
int Point::getColumn() const
{
	return (column_);
}

void Point::setColumn(const int &column)
{
	this->column_ = column;
}

int Point::getRow() const
{
	return (row_);
}

void Point::setRow(const int &row)
{
	this->row_ = row;
}

std::string Point::pointToString()
{
	std::stringstream string_stream;
	string_stream << "(" << getRow() << "," << getColumn() << ")";
	std::string s = string_stream.str();
	return (s);
}

bool Point::operator ==(const Point& other) const
{
	if ((other.getColumn() == this->getColumn())
			&& (other.getRow() == this->getRow()))
	{
		return (true);
	}
	return (false);
}

bool Point::operator !=(const Point& other) const
{
	if ((other.getColumn() != this->getColumn())
			|| (other.getRow() != this->getRow()))
	{
		return (true);
	}
	return (false);
}
