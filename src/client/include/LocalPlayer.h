/*
 * LocalPlayer.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef LOCALPLAYER_H_
#define LOCALPLAYER_H_
#include "Player.h"
#include <iostream>
class LocalPlayer: public Player
{
		friend class RemotePlayer;
	private:
		int player_symbol_;
		int opponent_symbol_;
		bool player_turn_;
		void setPlayerSymbol(const int &player_symbol);
		void setOpponentSymbol(const int &opponent_symbol);

		/**
		 * Function Name: getValidMoveIndex
		 * Description: checks if the move the user entered is a valid player move.
		 * Parameters:player_moves-a vector containing all strike_vector, each
		 * strike_vector contains the game pieces to be fliped in the strike path.
		 * Return Value:move_index - the index to chosen move is player moves.
		 */
		int getValidMoveIndex(
				const std::vector<std::vector<GamePiece> >& player_moves);

		/**
		 * Function Name: getUserInput
		 * Description: asks for user input - player next move, 2 numbers.
		 * Parameters: void.
		 * Return Value:point representing player next move.
		 */
		Point getUserInput();

		/**
		 * Function Name: printPlayerMoves
		 * Description: print the player possible next moves in format.
		 * Parameters:player_moves-a vector containing all strike_vector, each
		 * strike_vector contains the game pieces to be fliped in the strike path.
		 * Return Value:void.
		 */
		void printPlayerMoves(
				std::vector<std::vector<GamePiece> >& player_moves);
	public:
		LocalPlayer(const int &symbol);
		LocalPlayer(const Player &other_player);
		void playOneMove(std::vector<std::vector<GamePiece> >& player_moves,
				Board &game_board);
		const int getPlayerSymbol() const;
		const int getOpponentSymbol() const;
		bool isPlayerTurn() const;
		void setPlayerTurn(const bool &player_turn);
		~LocalPlayer();
};

#endif /* LOCALPLAYER_H_ */
