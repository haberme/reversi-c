/*
 * ConsolGame.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef CONSOLGAME_H_
#define CONSOLGAME_H_

#include "Game.h"

class ConsolGame: public Game
{
		friend class RemoteGame;
	private:
		const int row_size_;
		const int column_size_;
		GameRules *rules_;
		Board * game_board_;
		Player *player_1_;
		Player *player_2_;
		void playOneTurn();
		Player*& getPlayer1();
		Player*& getPlayer2();
		Board*& getGameBoard();
		const int getColumnSize() const;
		const int getRowSize() const;
		GameRules*& getGameRules();
		void gameOver();


	public:
		ConsolGame(const int &row, const int &column, Player *&p1, Player *&p2);
		void play();
		~ConsolGame();
};

#endif /* CONSOLGAME_H_ */
