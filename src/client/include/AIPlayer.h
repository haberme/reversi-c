/*
 * AIPlayer.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef AIPLAYER_H_
#define AIPLAYER_H_
#include "LocalPlayer.h"
class AIPlayer: public LocalPlayer
{
	public:
		/**
		 *Function Name: AIPlayer.
		 *Description: The AIPlayer constructor, creates the AIPlayer with the
		 *             given Player symbol.
		 *Parameters:symbol - the player symbol.
		 *Return Value: void.
		 */
		AIPlayer(const int &symbol);

		/**
		 *Function Name: AIPlayer.
		 *Description: The AIPlayer copy constructor, creates the AIPlayer from the
		 *             given Player.
		 *other_player - the player to copy.
		 *Return Value: void.
		 */
		AIPlayer(const Player &other_player);
		void playOneMove(std::vector<std::vector<GamePiece> >& player_moves,
				Board &game_board);
		/**
		 *Function Name: ~AIPlayer.
		 *Description: The AIPlayer destructor.
		 *Parameters:voidl.
		 *Return Value: void.
		 */
		~AIPlayer();
};

#endif /* LOCALPLAYER_H_ */
