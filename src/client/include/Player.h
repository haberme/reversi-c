/*
 * Player.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef PLAYER_H_
#define PLAYER_H_
#include "GamePiece.h"
#include "Board.h"
#include <vector>
class Player
{
	private:
		/**
		 *Function Name: setPlayerSymbol
		 *Description: sets the player symbol to given symbol.
		 *Parameters: player_symbol- the symbol to set as player symbol.
		 *Return Value: void
		 */
		virtual void setPlayerSymbol(const int &player_symbol)=0;

		/**
		 *Function Name: setOpponentSymbol
		 *Description: sets the opponent symbol to given symbol.
		 *Parameters: opponent_symbol- the symbol to set as opponent symbol.
		 *Return Value: void
		 */
		virtual void setOpponentSymbol(const int &opponent_symbol)=0;

	public:

		/**
		 *Function Name: Player
		 *Description: construct player with given symbol.
		 *Parameters: symbol- the symbol to set as player symbol.
		 *Return Value: void
		 */
		Player(const int &symbol)
		{
		}
		;
		/**
		 *Function Name: Player
		 *Description: copy constructor for player.
		 *Parameters: other_player- the player to copy..
		 *Return Value: void
		 */
		Player(const Player &other_player)
		{
		}
		;
		/**
		 *Function Name: playOneMove
		 *Description: play`s one player move.
		 * Parameters:possible_moves-a vector containing all strike_vector, each
		 * strike_vector contains the game pieces to be fliped in the strike path.
		 *Parameters: game_board- the game`s game board.
		 *Return Value: void
		 */
		virtual void playOneMove(
				std::vector<std::vector<GamePiece> >& player_moves,
				Board &game_board)=0;
		/**
		 *Function Name: getPlayerSymbol
		 *Description: returns the player symbol .
		 *Parameters: void
		 *Return Value: player_symbol- the symbol representing the player.
		 */
		virtual const int getPlayerSymbol() const=0;

		/**
		 *Function Name: getOpponentSymbol
		 *Description: returns the opponent symbol .
		 *Parameters: void
		 *Return Value: opponent_symbol- the symbol representing the opponent.
		 */
		virtual const int getOpponentSymbol() const=0;

		/**
		 *Function Name: isPlayerTurn
		 *Description: returns true if its the player turn.
		 *Parameters: void.
		 *Return Value: returns true if its the player turn.
		 */
		virtual bool isPlayerTurn() const=0;

		/**
		 *Function Name: setPlayerTurn
		 *Description: sets the player turn to given value(true/false).
		 *Parameters: player_turn - the value(true/false) to be set.
		 *Return Value: void.
		 */
		virtual void setPlayerTurn(const bool &player_turn)=0;

		/**
		 *Function Name: ~player
		 *Description: destructor - deletes all created elements.
		 *Parameters: void.
		 *Return Value: void
		 */
		virtual ~Player()
		{
		}
		;
};

#endif /* PLAYER_H_ */
