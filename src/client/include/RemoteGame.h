/*
 * RemoteGame.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef REMOTEGAME_H_
#define REMOTEGAME_H_
#include "ConsolGame.h"
class RemoteGame: public ConsolGame
{
	private:
		/**
		 * Function Name: playOneTurn
		 * Description: plays one turn in the game.
		 * Parameters: turn_number - the number of turn played.
		 * Return Value:void.
		 */
		void playOneTurn(int& turn_number);
		/**
		 * Function Name: playOneGivenMove
		 * Description: plays the move at the given point move to play and prints board.
		 * Parameters:possible_moves-a vector containing all strike_vector, each
		 * strike_vector contains the game pieces to be fliped in the strike path.
		 * Parameters: move_to_play - the point representing the move to play on game board.
		 * Return Value:void.
		 */
		void playOneGivenMove(Point& move_to_play,
				std::vector<std::vector<GamePiece> >& possible_moves);
		/**
		 * Function Name: checkGameOver
		 * Description: checks if the game over conditions have been met.
		 * Parameters: void.
		 * Return Value:false if game is not over true otherwise.
		 */
		bool checkGameOver();
	public:
		RemoteGame(const int &row, const int &column, Player *&p1, Player *&p2);
		void play();
		virtual ~RemoteGame();
};

#endif /* REMOTEGAME_H_ */
