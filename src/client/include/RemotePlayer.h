/*
 * RemotePlayer.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef SERVER_REMOTEPLAYER_H_
#define SERVER_REMOTEPLAYER_H_
#include "LocalPlayer.h"
#include "SocketConnectionStatus.h"
#include <iostream>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<string.h>
#include<unistd.h>
class RemotePlayer: public LocalPlayer
{
	private:
		const
		char * server_IP_;
		int server_port_;
		int client_socket_;
		Point opponent_move_;

		/**
		 *Function Name: setGameOverMove
		 *Description: sets the Point opponent_move_ to Point(game_over,game_over).
		 *Parameters: void.
		 *Return Value: void.
		 */
		void setGameOverMove();
		/**
		 *Function Name: setRemoteOpponentMove
		 *Description: sets the remote opponent played move to given point.
		 *Parameters: opponentMove - the remote opponent played move.
		 *Return Value: void.
		 */
		void setRemoteOpponentMove(Point& opponentMove);
		/**
		 *Function Name: getClientSocket
		 *Description: returns this remote player client socket.
		 *Parameters: void.
		 *Return Value: client_socket_ - needed to communicate with server.
		 */
		int getClientSocket() const;
		/**
		 *Function Name: setClientSocket
		 *Description: sets this remote player client socket to given value.
		 *Parameters:client_socket_ - needed to communicate with server.
		 *Return Value: void.
		 */
		void setClientSocket(const int& clientSocket);
		/**
		 *Function Name: getServerIp
		 *Description: returns this remote player server ip.
		 *Parameters: void.
		 *Return Value: server_Ip_ - the server address ,needed to communicate with server.
		 */
		const char* getServerIp() const;
		/**
		 *Function Name: setServerIp
		 *Description: sets this remote player server ip to given value.
		 *Parameters:serverIp- the server address ,needed to communicate with server.
		 *Return Value: void.
		 */
		void setServerIp(const char* serverIp);
		/**
		 *Function Name: getServerPort
		 *Description: returns this remote player server port.
		 *Parameters: void.
		 *Return Value: server_Port - the server port ,needed to communicate with server.
		 */
		int getServerPort() const;
		/**
		 *Function Name: setServerPort
		 *Description: sets this remote player server port to given value.
		 *Parameters:serverPort- the server port ,needed to communicate with server.
		 *Return Value: void.
		 */
		void setServerPort(const int& serverPort);

		/**
		 *Function Name: writeToServer
		 *Description: sends given data to  game server .
		 *Parameters:data_to_write - the data we want to send to server.
		 *Return Value: a value representing the write status (error/disconnected/success).
		 */
		const int writeToServer(const int &data_to_write);
		/**
		 *Function Name: readFromServer
		 *Description: reads data from game server .
		 *Parameters:void.
		 *Return Value: a value representing the data sent from server or
		 * the read status (error/disconnected).
		 */
		const int readFromServer();

		/**
		 *Function Name: connectToServer
		 *Description: connects this remote player to game server.
		 *Parameters: opponent - the player opponent.
		 *Return Value: void.
		 */
		void connectToServer(Player*& opponent);
	public:
		virtual ~RemotePlayer();
		/**
		 *Function Name: playOneMove
		 *Description: play`s one player move.
		 * Parameters:possible_moves-a vector containing all strike_vector, each
		 * strike_vector contains the game pieces to be fliped in the strike path.
		 *Parameters: game_board- the game`s game board.
		 *Parameters: turn_number- the number of turns played so far in game.
		 *Return Value: void
		 */
		void playOneMove(std::vector<std::vector<GamePiece> >& player_moves,
				Board &game_board,int& turn_number);
		/**
		 *Function Name: RemotePlayer
		 *Description: create a RemotePlayer with ability to play against another Remote Player .
		 *Parameters: server_IP-the server address ,needed to communicate with server.
		 *Parameters: server_port -the server port ,needed to communicate with server.
		 *Parameters: opponent - the local player opponent.
		 *Return Value:void.
		 */
		RemotePlayer(const char * &server_IP, const int &server_port,Player*& opponent);
		/**
		 *Function Name: getRemoteOpponentMove
		 *Description: returns the remote opponent  played move.
		 *Parameters: void.
		 *Return Value: opponent_move_ - the remote opponent played move.
		 */
		Point& getRemoteOpponentMove();
		/**
		 *Function Name: sendGameOverSignal
		 *Description: sends the server game over signal.
		 *Parameters: void.
		 *Return Value: void.
		 */
		void sendGameOverSignal();
		/**
		 *Function Name: readRemoteOpponentMove
		 *Description: reads the remote opponent move from the server, and save it to Point opponent_move_.
		 *Parameters: turn_number - the number of turns played so far.
		 *Parameters: is_game_over - true if game over conditions are met false otherwise.
		 *Return Value: void.
		 */
		void readRemoteOpponentMove(int& turn_number,bool is_game_over);
		/**
		 *Function Name: writeNoMove
		 *Description: sends the server no move signal.
		 *Parameters: void.
		 *Return Value: void.
		 */
		void writeNoMove();

};

#endif /* SERVER_REMOTEPLAYER_H_ */
