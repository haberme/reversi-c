#ifndef IPPORTREADER_H_
#define IPPORTREADER_H_

#include <sstream>
#include <fstream>
#include <cstring>
#include <exception>
#include <iostream>
using namespace std;

#define PORT_LOWER_LIMMIT 1025
#define PORT_UPPER_LIMMIT 65535
#define IP_LOWER_LIMMIT 0
#define IP_UPPER_LIMMIT 255

// self define exception for a IP entry problem such as out of bound number.
// NOTE: the exception handle wrong entry not bad connection/avilabe socket.
struct IPError : public exception
{
	const char * what () const throw ()
	{
		return "The IP is not a number between 0-255 or missing.";
	}
};

// self define exception for a port entry problem such as out of bound number.
// NOTE: the exception handle wrong entry not bad connection/avilabe socket.
struct PortError : public exception
{
	const char * what () const throw ()
	{
		return "The port is not a number between 0-255 or missing.";
	}
};

// self define exception for non existing file.
struct FileNotExist : public exception
{
	const char * what () const throw()
	{
		return "The given file does not exist.";
	}
};

// struct contain the IP and port info.
struct IPPortSettings
{
	string IP;
	int port;
};

/**
 *Function Name: readFile.
 *Description: get the IP and port from given file.
 *Parameters: the file location as const char*.
 *Return Value: IP and port settings as IPPortSettings struct.
 */
IPPortSettings readFile(const char *path);

/**
 *Function Name: stringToNumber.
 *Description: get number from a string.
 *Parameters: number represented as a string.
 *Return Value: the number or -1 if the string is not a number.
 */
int stringToNumber(string stringNumber);

#endif