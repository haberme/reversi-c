/*
 * Game.h - game interface.
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef GAME_H_
#define GAME_H_
#include <iostream>
#include "Board.h"
#include "LocalPlayer.h"
#include "AIPlayer.h"
#include "RemotePlayer.h"

class Game
{
	private:
		/**
		 * Function Name: playOneTurn
		 * Description: plays one player turn in the game.
		 * Parameters:void.
		 * Return Value:void.
		 */
		virtual void playOneTurn()=0;

		/**
		 * Function Name: getPlayer1
		 * Description: returns player 1.
		 * Parameters:void.
		 * Return Value:player 1 - the first player.
		 */
		virtual Player*& getPlayer1()=0;

		/**
		 * Function Name: getPlayer2
		 * Description: returns player 2.
		 * Parameters:void.
		 * Return Value:player 2 - the second player.
		 */
		virtual Player*& getPlayer2()=0;

		/**
		 * Function Name: getGameBoard
		 * Description: returns the game board .
		 * Parameters:void.
		 * Return Value:game board - the board the game is player on.
		 */
		virtual Board*& getGameBoard()=0;

		/**
		 * Function Name: getColumnSize
		 * Description: returns the game board column size.
		 * Parameters:void.
		 * Return Value:column size - the game board column size.
		 */
		virtual const int getColumnSize() const=0;

		/**
		 * Function Name: getRowSize
		 * Description: returns the game board row size.
		 * Parameters:void.
		 * Return Value:row size - the game board row size.
		 */
		virtual const int getRowSize() const=0;

		/**
		 * Function Name: getGameRules
		 * Description: returns the game rules.
		 * Parameters:void.
		 * Return Value:game rules- the rules dictating which moves are valid
		 *  and the game over terms.
		 */
		virtual GameRules*& getGameRules()=0;

		/**
		 * Function Name: gameOver
		 * Description: invoked when the game over terms are met,prints winner.
		 * Parameters:void.
		 * Return Value:void.
		 */
		virtual void gameOver()=0;


	public:
		/**
		 * Function Name: Game
		 * Description: Game constructor.
		 * Parameters:row - the number of rows on the game board.
		 * Parameters:column - the number of columns on the game board.
		 * Parameters:p1 - the game first player.
		 * Parameters:p2 - the game second player.
		 * Return Value:void.
		 */
		Game(const int &row, const int &column, Player *&p1, Player *&p2)
		{
		}
		;

		/**
		 * Function Name: play
		 * Description: invoked after the game is constructed, starts the game.
		 * Parameters:void.
		 * Return Value:void.
		 */
		virtual void play()=0;
		/**
		 * Function Name: ~Game
		 * Description: Game destructor - deletes all created game elements.
		 * Parameters:void.
		 * Return Value:void.
		 */
		virtual ~Game()
		{
		}
		;
};

#endif /* GAME_H_ */
