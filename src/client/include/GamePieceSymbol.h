/*
 * GamePieceSymbol.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */
#ifndef GAMEPIECESYMBOL_H_
#define GAMEPIECESYMBOL_H_
enum GamePieceSymbol
{
	player_1 = 'X', player_2 = 'O', empty = ' '
};

#endif /* GAMEPIECESYMBOL_H_ */
