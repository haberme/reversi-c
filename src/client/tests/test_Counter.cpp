/*
 * test_Counter.cpp
 *
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/Counter.h"
#include "../include/GamePieceSymbol.h"
#include <iostream>
using namespace std;

class CounterTest: public ::testing::Test
{
	protected:
		Counter nonset;
		Counter * pointerNonset;
		Counter preset;

		virtual void SetUp()
		{
			cout << "setting up...";
			preset.setEmptyCells(42);
			preset.addToPlayer(empty, player_1);
			preset.addToPlayer(empty, player_1);
			preset.addToPlayer(empty, player_1);
			preset.addToPlayer(empty, player_2);
			preset.addToPlayer(empty, player_2);
			preset.addToPlayer(player_2, player_1);
			preset.addToPlayer(player_2, player_1);
			preset.addToPlayer(player_2, player_1);
			preset.addToPlayer(player_2, player_1);
			preset.addToPlayer(player_2, player_1);
			cout << "finished setting up." << endl;
		}

		virtual void TearDown()
		{
			cout << "tearing down...";
			delete pointerNonset;
			cout << "finished tearing down." << endl;
		}

	public:
		CounterTest() :
				nonset(), preset()
		{
			pointerNonset = new Counter();
		}
		;
};

TEST_F(CounterTest, GettingBoardScores)
{
	EXPECT_EQ(0, nonset.getBoardScore(player_1));
	EXPECT_EQ(0, nonset.getBoardScore(player_2));
	EXPECT_EQ(0, pointerNonset->getBoardScore(player_1));
	EXPECT_EQ(0, pointerNonset->getBoardScore(player_2));
	EXPECT_EQ(11, preset.getBoardScore(player_1));
	EXPECT_EQ(-11, preset.getBoardScore(player_2));
}

TEST_F(CounterTest, GettingPlayersScores)
{
	EXPECT_EQ(0, nonset.getPlayer1Score());
	EXPECT_EQ(0, nonset.getPlayer2Score());
	EXPECT_EQ(0, pointerNonset->getPlayer1Score());
	EXPECT_EQ(0, pointerNonset->getPlayer2Score());
	EXPECT_EQ(8, preset.getPlayer1Score());
	EXPECT_EQ(-3, preset.getPlayer2Score());
}

TEST_F(CounterTest, GettingEmptyCells)
{
	EXPECT_EQ(0, nonset.getEmptyCells());
	EXPECT_EQ(0, pointerNonset->getEmptyCells());
	EXPECT_EQ(37, preset.getEmptyCells());
}

TEST_F(CounterTest, CopyConstructor)
{
	Counter copy1(preset);
	EXPECT_EQ(preset.getBoardScore(player_1), copy1.getBoardScore(player_1));
	EXPECT_EQ(preset.getBoardScore(player_2), copy1.getBoardScore(player_2));
	EXPECT_EQ(preset.getPlayer1Score(), copy1.getPlayer1Score());
	EXPECT_EQ(preset.getPlayer2Score(), copy1.getPlayer2Score());
	EXPECT_EQ(preset.getEmptyCells(), copy1.getEmptyCells());

	Counter copy2(nonset);
	EXPECT_EQ(nonset.getBoardScore(player_1), copy2.getBoardScore(player_1));
	EXPECT_EQ(nonset.getBoardScore(player_2), copy2.getBoardScore(player_2));
	EXPECT_EQ(nonset.getPlayer1Score(), copy2.getPlayer1Score());
	EXPECT_EQ(nonset.getPlayer2Score(), copy2.getPlayer2Score());
	EXPECT_EQ(nonset.getEmptyCells(), copy2.getEmptyCells());

	Counter copy3(*pointerNonset);
	EXPECT_EQ(pointerNonset->getBoardScore(player_1),
			copy3.getBoardScore(player_1));
	EXPECT_EQ(pointerNonset->getBoardScore(player_2),
			copy3.getBoardScore(player_2));
	EXPECT_EQ(pointerNonset->getPlayer1Score(), copy3.getPlayer1Score());
	EXPECT_EQ(pointerNonset->getPlayer2Score(), copy3.getPlayer2Score());
	EXPECT_EQ(pointerNonset->getEmptyCells(), copy3.getEmptyCells());

	Counter *copy4 = new Counter(preset);
	EXPECT_EQ(preset.getBoardScore(player_1), copy4->getBoardScore(player_1));
	EXPECT_EQ(preset.getBoardScore(player_2), copy4->getBoardScore(player_2));
	EXPECT_EQ(preset.getPlayer1Score(), copy4->getPlayer1Score());
	EXPECT_EQ(preset.getPlayer2Score(), copy4->getPlayer2Score());
	EXPECT_EQ(preset.getEmptyCells(), copy4->getEmptyCells());

	delete copy4;
	cout << endl << endl;
}
