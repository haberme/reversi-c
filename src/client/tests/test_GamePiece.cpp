/*
 * test_GamePiece.cpp
 * 
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/GamePiece.h"
#include "../include/GamePieceSymbol.h"
#include <iostream>
using namespace std;

class GamePieceTest: public ::testing::Test
{
	protected:
		GamePiece nonset;
		GamePiece preset;
		GamePiece startWithInts;
		GamePiece *pointerStartWithPoint;

		virtual void SetUp()
		{
			cout << "setting up...";
			Point p2(4, 2);
			preset.setPieceLocationPoint(p2);
			preset.setGamePieceSymbol(player_2);
			cout << "finished setting up." << endl;
		}

		virtual void TearDown()
		{
			cout << "tearing down...";
			delete pointerStartWithPoint;
			cout << "finished tearing down." << endl;
		}

	public:
		GamePieceTest() :
				nonset(), preset(), startWithInts(4, 2, player_1)
		{
			Point p1(4, 2);
			pointerStartWithPoint = new GamePiece(p1, player_1);
		}
		;
};

TEST_F(GamePieceTest, GettingRowValues)
{
	EXPECT_EQ(-1, nonset.getGamePieceRow());
	EXPECT_EQ(4, preset.getGamePieceRow());
	EXPECT_EQ(4, startWithInts.getGamePieceRow());
	EXPECT_EQ(4, pointerStartWithPoint->getGamePieceRow());
}

TEST_F(GamePieceTest, GettingColumnValues)
{
	EXPECT_EQ(-1, nonset.getGamePieceColumn());
	EXPECT_EQ(2, preset.getGamePieceColumn());
	EXPECT_EQ(2, startWithInts.getGamePieceColumn());
	EXPECT_EQ(2, pointerStartWithPoint->getGamePieceColumn());
}

TEST_F(GamePieceTest, GettingPoints)
{
	EXPECT_EQ(-1, nonset.getGamePieceRow());
	EXPECT_EQ(4, preset.getGamePieceRow());
	EXPECT_EQ(4, startWithInts.getGamePieceRow());
	EXPECT_EQ(4, pointerStartWithPoint->getGamePieceRow());
}

TEST_F(GamePieceTest, GettingSymbolValues)
{
	EXPECT_EQ(empty, nonset.getGamePieceSymbol());
	EXPECT_EQ(player_2, preset.getGamePieceSymbol());
	EXPECT_EQ(player_1, startWithInts.getGamePieceSymbol());
	EXPECT_EQ(player_1, pointerStartWithPoint->getGamePieceSymbol());
}

TEST_F(GamePieceTest, EquelSignTest)
{
	Point p1(-1, -1);
	Point p2(4, 2);
	Point p3(4, 2);
	Point p4(4, 2);
	EXPECT_TRUE(p1 == nonset.getPieceLocationPoint());
	EXPECT_TRUE(p2 == preset.getPieceLocationPoint());
	EXPECT_TRUE(p3 == startWithInts.getPieceLocationPoint());
	EXPECT_TRUE(p4 == pointerStartWithPoint->getPieceLocationPoint());
}

TEST_F(GamePieceTest, GamePieceToStringTest)
{
	EXPECT_EQ("game piece at point: (-1,-1)  is :  ",
			nonset.gamePieceToString());
	EXPECT_EQ("game piece at point: (4,2)  is : O", preset.gamePieceToString());
	EXPECT_EQ("game piece at point: (4,2)  is : X",
			startWithInts.gamePieceToString());
	EXPECT_EQ("game piece at point: (4,2)  is : X",
			pointerStartWithPoint->gamePieceToString());
}

TEST_F(GamePieceTest, CopyConstructor)
{
	GamePiece copy1(nonset);
	EXPECT_EQ(copy1.getGamePieceSymbol(), nonset.getGamePieceSymbol());
	EXPECT_TRUE(
			copy1.getPieceLocationPoint() == nonset.getPieceLocationPoint());

	GamePiece copy2(preset);
	EXPECT_EQ(copy2.getGamePieceSymbol(), preset.getGamePieceSymbol());
	EXPECT_TRUE(
			copy2.getPieceLocationPoint() == preset.getPieceLocationPoint());

	GamePiece copy3(startWithInts);
	EXPECT_EQ(copy3.getGamePieceSymbol(), startWithInts.getGamePieceSymbol());
	EXPECT_TRUE(
			copy3.getPieceLocationPoint()
					== startWithInts.getPieceLocationPoint());

	GamePiece copy4(*pointerStartWithPoint);
	EXPECT_EQ(copy4.getGamePieceSymbol(),
			pointerStartWithPoint->getGamePieceSymbol());
	EXPECT_TRUE(
			copy4.getPieceLocationPoint()
					== pointerStartWithPoint->getPieceLocationPoint());

	GamePiece *copy5 = new GamePiece(nonset);
	EXPECT_EQ(copy5->getGamePieceSymbol(), nonset.getGamePieceSymbol());
	EXPECT_TRUE(
			copy5->getPieceLocationPoint() == nonset.getPieceLocationPoint());

	GamePiece *copy6 = new GamePiece(preset);
	EXPECT_EQ(copy6->getGamePieceSymbol(), preset.getGamePieceSymbol());
	EXPECT_TRUE(
			copy6->getPieceLocationPoint() == preset.getPieceLocationPoint());

	GamePiece *copy7 = new GamePiece(startWithInts);
	EXPECT_EQ(copy7->getGamePieceSymbol(), startWithInts.getGamePieceSymbol());
	EXPECT_TRUE(
			copy7->getPieceLocationPoint()
					== startWithInts.getPieceLocationPoint());

	GamePiece *copy8 = new GamePiece(*pointerStartWithPoint);
	EXPECT_EQ(copy8->getGamePieceSymbol(),
			pointerStartWithPoint->getGamePieceSymbol());
	EXPECT_TRUE(
			copy8->getPieceLocationPoint()
					== pointerStartWithPoint->getPieceLocationPoint());

	delete copy5;
	delete copy6;
	delete copy7;
	delete copy8;
	cout << endl << endl;

}
