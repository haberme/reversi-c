/*
 * test_BasicGameRules.cpp
 *
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/BasicGameRules.h"
#include <iostream>
using namespace std;

class BasicGameRulesTest: public ::testing::Test
{
	protected:
		BasicGameRules nonset;
		BasicGameRules preset;
		Counter *counter;
		std::string rule;
		std::string basic_rule_description;
		BasicGameRules * pointerNonset;

		virtual void SetUp()
		{
			cout << "setting up...";
			rule = "The first rule is that there are no Rules!!";
			basic_rule_description =
					"Basic game rules: "
							"1)player can only do moves if there is a empty cell at the end of his strike direction"
							"2)can`t finish a player strike if it has the player pieces in it."
							"3)invalid move - if the piece is at the edge of board and isn`t empty."
							"4)invalid move - if got to empty cell without encountering opponent.";
			preset = BasicGameRules(rule);
			//counter - empty=5 | player_1 = 4|player_2=1.
			counter->setEmptyCells(10);
			counter->addToPlayer(empty, player_1);
			counter->addToPlayer(empty, player_1);
			counter->addToPlayer(empty, player_1);
			counter->addToPlayer(empty, player_2);
			counter->addToPlayer(empty, player_2);
			counter->addToPlayer(player_2, player_1);
			cout << "finished setting up." << endl;
		}

		virtual void TearDown()
		{
			cout << "tearing down...";
			delete pointerNonset;
			delete counter;
			cout << "finished tearing down." << endl;
		}

	public:
		BasicGameRulesTest() :
				nonset(), preset(), counter(new Counter()), rule(
						"The first rule is that there are no Rules!!"), basic_rule_description(
						"Basic game rules: "
								"1)player can only do moves if there is a empty cell at the end of his strike direction"
								"2)can`t finish a player strike if it has the player pieces in it."
								"3)invalid move - if the piece is at the edge of board and isn`t empty."
								"4)invalid move - if got to empty cell without encountering opponent.")
		{
			pointerNonset = new BasicGameRules();

		}
};
TEST_F(BasicGameRulesTest, Getting_BasicGameRules_rule_description)
{
	EXPECT_EQ(basic_rule_description, nonset.getRuleDescription());
	EXPECT_EQ(rule, preset.getRuleDescription());
	EXPECT_EQ(basic_rule_description, pointerNonset->getRuleDescription());
}
TEST_F(BasicGameRulesTest, checkValidMove_BasicGameRules)
{
	//checkValidMove(current_piece, player_symbol, opponent_encounterd, is_Board_Edge_piece)
	bool f = false;
	bool t = true;
	//can`t finish a player strike if it has the player pieces in it.
	EXPECT_FALSE(preset.checkValidMove(player_1, player_1, t, t));
	EXPECT_FALSE(preset.checkValidMove(player_1, player_1, f, f));
	EXPECT_FALSE(preset.checkValidMove(player_1, player_1, t, f));
	EXPECT_FALSE(preset.checkValidMove(player_1, player_1, f, t));
	EXPECT_FALSE(nonset.checkValidMove(player_1, player_1, t, t));
	EXPECT_FALSE(nonset.checkValidMove(player_1, player_1, f, f));
	EXPECT_FALSE(nonset.checkValidMove(player_1, player_1, t, f));
	EXPECT_FALSE(nonset.checkValidMove(player_1, player_1, f, t));
	EXPECT_FALSE(pointerNonset->checkValidMove(player_1, player_1, t, t));
	EXPECT_FALSE(pointerNonset->checkValidMove(player_1, player_1, f, f));
	EXPECT_FALSE(pointerNonset->checkValidMove(player_1, player_1, t, f));
	EXPECT_FALSE(pointerNonset->checkValidMove(player_1, player_1, f, t));
	// invalid move - if the piece is at the edge of board and isn`t empty.
	EXPECT_FALSE(preset.checkValidMove(player_1, player_2, t, t));
	EXPECT_FALSE(preset.checkValidMove(player_1, player_2, f, t));
	EXPECT_FALSE(nonset.checkValidMove(player_1, player_2, t, t));
	EXPECT_FALSE(nonset.checkValidMove(player_1, player_2, f, t));
	EXPECT_FALSE(pointerNonset->checkValidMove(player_1, player_2, t, t));
	EXPECT_FALSE(pointerNonset->checkValidMove(player_1, player_2, f, t));
	// got to empty cell without encountering opponent.
	EXPECT_FALSE(preset.checkValidMove(empty, player_1, f, f));
	EXPECT_FALSE(preset.checkValidMove(empty, player_1, f, t));
	EXPECT_FALSE(nonset.checkValidMove(empty, player_1, f, f));
	EXPECT_FALSE(nonset.checkValidMove(empty, player_1, f, t));
	EXPECT_FALSE(pointerNonset->checkValidMove(empty, player_1, f, f));
	EXPECT_FALSE(pointerNonset->checkValidMove(empty, player_1, f, t));
	//valid move - encountered opponent and found empty cell.
	EXPECT_TRUE(preset.checkValidMove(empty, player_1, t, t));
	EXPECT_TRUE(preset.checkValidMove(empty, player_1, t, f));
	EXPECT_TRUE(nonset.checkValidMove(empty, player_1, t, t));
	EXPECT_TRUE(nonset.checkValidMove(empty, player_1, t, f));
	EXPECT_TRUE(pointerNonset->checkValidMove(empty, player_1, t, t));
	EXPECT_TRUE(pointerNonset->checkValidMove(empty, player_1, t, f));
}
TEST_F(BasicGameRulesTest, getWinnerScore_BasicGameRules)
{
	//counter - empty=5 | player_1 = 4|player_2=1.
	const Counter *c = counter;
	bool f = false;
	bool t = true;
	preset.isGameOver(c, t, f);
	nonset.isGameOver(c, t, f);
	pointerNonset->isGameOver(c, t, f);
	EXPECT_EQ(4, preset.getWinnerScore());
	EXPECT_EQ(4, nonset.getWinnerScore());
	EXPECT_EQ(4, pointerNonset->getWinnerScore());
}
TEST_F(BasicGameRulesTest, getWinnerSymbol_BasicGameRules)
{
	//counter - empty=5 | player_1 = 4|player_2=1.
	const Counter *c = counter;
	bool f = false;
	bool t = true;
	preset.isGameOver(c, t, f);
	nonset.isGameOver(c, t, f);
	pointerNonset->isGameOver(c, t, f);
	EXPECT_EQ(player_1, preset.getWinnerSymbol());
	EXPECT_EQ(player_1, nonset.getWinnerSymbol());
	EXPECT_EQ(player_1, pointerNonset->getWinnerSymbol());
}
TEST_F(BasicGameRulesTest, isGameOver_BasicGameRules)
{
	//counter - empty=5 | player_1 = 4|player_2=1.
	const Counter *c = counter;
	bool f = false;
	bool t = true;
	EXPECT_FALSE(preset.isGameOver(c, t, f));
	EXPECT_FALSE(nonset.isGameOver(c, t, f));
	EXPECT_FALSE(pointerNonset->isGameOver(c, t, f));
	EXPECT_FALSE(preset.isGameOver(c, f, t));
	EXPECT_FALSE(nonset.isGameOver(c, f, t));
	EXPECT_FALSE(pointerNonset->isGameOver(c, f, t));
	EXPECT_TRUE(preset.isGameOver(c, f, f));
	EXPECT_TRUE(nonset.isGameOver(c, f, f));
	EXPECT_TRUE(pointerNonset->isGameOver(c, f, f));
	cout << endl << endl;
}
