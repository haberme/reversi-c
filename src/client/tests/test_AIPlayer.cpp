/*
 * test_AIPlayer.cpp
 *
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/AIPlayer.h"
#include <iostream>
using namespace std;

class AIPlayerTest: public ::testing::Test
{

	protected:
		AIPlayer nonset;
		AIPlayer * pointerNonset;
		AIPlayer preset;
		virtual void SetUp()
		{
			cout << "setting up...";
			preset.setPlayerTurn(true);
			cout << "finished setting up." << endl;
		}

		virtual void TearDown()
		{
			cout << "tearing down...";
			delete pointerNonset;
			cout << "finished tearing down." << endl;
		}

	public:
		AIPlayerTest() :
				nonset(player_2), preset(player_2)
		{
			pointerNonset = new AIPlayer(player_2);
		}
		;
};
TEST_F(AIPlayerTest, GettingAIPlayer_PlayerSymbol)
{
	EXPECT_EQ(player_2, nonset.getPlayerSymbol());
	EXPECT_EQ(player_2, pointerNonset->getPlayerSymbol());
	EXPECT_EQ(player_2, preset.getPlayerSymbol());
}
TEST_F(AIPlayerTest, GettingAIPlayer_OpponentSymbol)
{
	EXPECT_EQ(player_1, nonset.getOpponentSymbol());
	EXPECT_EQ(player_1, pointerNonset->getOpponentSymbol());
	EXPECT_EQ(player_1, preset.getOpponentSymbol());
}
TEST_F(AIPlayerTest, GettingAIPlayer_Turn_)
{
	EXPECT_EQ(false, nonset.isPlayerTurn());
	EXPECT_EQ(false, pointerNonset->isPlayerTurn());
	EXPECT_EQ(true, preset.isPlayerTurn());
}
TEST_F(AIPlayerTest, SettingAIPlayer_Turn)
{
	nonset.setPlayerTurn(true);
	pointerNonset->setPlayerTurn(true);
	preset.setPlayerTurn(true);
	EXPECT_EQ(true, nonset.isPlayerTurn());
	EXPECT_EQ(true, pointerNonset->isPlayerTurn());
	EXPECT_EQ(true, preset.isPlayerTurn());
}
TEST_F(AIPlayerTest, AIPlayer_CopyConstructor)
{
	AIPlayer copy1(preset);
	EXPECT_EQ(preset.isPlayerTurn(), copy1.isPlayerTurn());
	EXPECT_EQ(preset.getPlayerSymbol(), copy1.getPlayerSymbol());
	EXPECT_EQ(preset.getOpponentSymbol(), copy1.getOpponentSymbol());
	copy1.setPlayerTurn(!preset.isPlayerTurn());
	EXPECT_EQ(!preset.isPlayerTurn(), copy1.isPlayerTurn());

	AIPlayer copy2(nonset);
	EXPECT_EQ(nonset.isPlayerTurn(), copy2.isPlayerTurn());
	EXPECT_EQ(nonset.getPlayerSymbol(), copy2.getPlayerSymbol());
	EXPECT_EQ(nonset.getOpponentSymbol(), copy2.getOpponentSymbol());
	copy2.setPlayerTurn(!nonset.isPlayerTurn());
	EXPECT_EQ(!nonset.isPlayerTurn(), copy2.isPlayerTurn());

	AIPlayer copy3(*pointerNonset);
	EXPECT_EQ(pointerNonset->isPlayerTurn(), copy3.isPlayerTurn());
	EXPECT_EQ(pointerNonset->getPlayerSymbol(), copy3.getPlayerSymbol());
	EXPECT_EQ(pointerNonset->getOpponentSymbol(), copy3.getOpponentSymbol());
	copy3.setPlayerTurn(!pointerNonset->isPlayerTurn());
	EXPECT_EQ(!pointerNonset->isPlayerTurn(), copy3.isPlayerTurn());

	AIPlayer *copy4 = new AIPlayer(preset);
	EXPECT_EQ(copy4->isPlayerTurn(), preset.isPlayerTurn());
	EXPECT_EQ(copy4->getPlayerSymbol(), preset.getPlayerSymbol());
	EXPECT_EQ(copy4->getOpponentSymbol(), preset.getOpponentSymbol());
	copy4->setPlayerTurn(!preset.isPlayerTurn());
	EXPECT_EQ(!preset.isPlayerTurn(), copy4->isPlayerTurn());

	delete copy4;
}
TEST_F(AIPlayerTest, AIPlayer_playOneMove)
{
	GameRules* rules = new BasicGameRules();
	Board* b = new Board(4, 4, rules);
	std::vector<std::pair<Point, int> > play_moves;
	play_moves.push_back(std::make_pair(Point(3, 2), player_1));
	play_moves.push_back(std::make_pair(Point(1, 3), player_2));
	play_moves.push_back(std::make_pair(Point(0, 3), player_1));
	b = b->createBoardWithMoves(play_moves, b);
	Board* b1 = new Board(*b);
	Board* b2 = new Board(*b);
	Board* b3 = new Board(*b);
	//play the O player move (3,1) on original b.
	play_moves.push_back(std::make_pair(Point(3, 1), player_2));
	b = b->createBoardWithMoves(play_moves, b);
	std::vector<std::vector<GamePiece> > player_moves1 =
			b1->getPlayerPossibleMoves(player_2, player_1);
	std::vector<std::vector<GamePiece> > player_moves2 =
			b2->getPlayerPossibleMoves(player_2, player_1);
	std::vector<std::vector<GamePiece> > player_moves3 =
			b3->getPlayerPossibleMoves(player_2, player_1);
	//player_2 the AI have 2 options (3,3) & (3,1) AI should choose (3,1).
	nonset.playOneMove(player_moves1, *b1);
	pointerNonset->playOneMove(player_moves2, *b2);
	preset.playOneMove(player_moves3, *b3);
	cout << "player_2 the AI have 2 options (3,3) & (3,1) AI should choose "
			"(3,1),if all 3 prints are the same we are OK" << endl;
	EXPECT_TRUE(*b == *b1);
	EXPECT_TRUE(*b == *b2);
	EXPECT_TRUE(*b == *b3);
	delete b;
	delete b1;
	delete b2;
	delete b3;
	delete rules;
	cout << endl << endl;
}
