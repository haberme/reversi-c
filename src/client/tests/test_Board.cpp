/*
 * test_Board.cpp
 * 
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/Board.h"
#include <iostream>
using namespace std;

class BoardTest: public ::testing::Test
{
	protected:
		Board *board1;
		Board *board2;
		Board *board3;
		Board *board4;
		GameRules *rules;

		virtual void SetUp()
		{
			cout << "setting up...";
			board4 = new Board(4, 4, rules);
			std::vector<std::pair<Point, int> > play_moves;
			play_moves.push_back(std::make_pair(Point(3, 2), player_1));
			play_moves.push_back(std::make_pair(Point(1, 3), player_2));
			play_moves.push_back(std::make_pair(Point(0, 3), player_1));
			board4 = board4->createBoardWithMoves(play_moves, board4);
			cout << "finished setting up." << endl;
		}

		virtual void TearDown()
		{
			cout << "tearing down...";
			delete rules;
			delete board1;
			delete board2;
			delete board3;
			delete board4;
			cout << "finished tearing down." << endl;
		}

	public:
		BoardTest() :
				board4(new Board(4, 4, rules))
		{
			rules = new BasicGameRules();
			board1 = new Board(8, 8, rules);
			board2 = new Board(4, 4, rules);
			board3 = new Board(4, 4, rules);
		}
};
TEST_F(BoardTest, Board_CopyConstructor)
{
	Board *board1_copy = new Board(*board1);
	Board *board2_copy = new Board(*board2);
	Board *board4_copy = new Board(*board4);

	EXPECT_TRUE(*board1 == *board1_copy);
	EXPECT_TRUE(*board2 == *board2_copy);
	EXPECT_TRUE(*board4 == *board4_copy);
	EXPECT_TRUE(*board4_copy != *board1_copy);
	EXPECT_TRUE(*board2_copy != *board4_copy);
	EXPECT_TRUE(*board1_copy != *board2_copy);
	EXPECT_FALSE(*board1_copy == *board2_copy);
	EXPECT_FALSE(*board1 == *board2_copy);
	EXPECT_FALSE(*board2 == *board1_copy);
	delete board1_copy;
	delete board2_copy;
	delete board4_copy;
}
TEST_F(BoardTest, Board_initializeBoard)
{
	EXPECT_TRUE(*board1 != *board2);
	EXPECT_TRUE(*board1 != *board3);
	EXPECT_TRUE(*board3 == *board2);
	EXPECT_FALSE(*board1 == *board2);
	EXPECT_FALSE(*board1 == *board3);
	EXPECT_FALSE(*board2 != *board3);
}
TEST_F(BoardTest, Board_operator_Equal)
{
	EXPECT_TRUE(*board3 == *board2);
	EXPECT_FALSE(*board1 == *board2);
	EXPECT_FALSE(*board1 == *board3);
	EXPECT_FALSE(*board1 == *board4);
	EXPECT_FALSE(*board2 == *board4);
	EXPECT_FALSE(*board3 == *board4);
}

TEST_F(BoardTest, Board_operator_Not_Equal)
{
	EXPECT_FALSE(*board3 != *board2);
	EXPECT_TRUE(*board1 != *board2);
	EXPECT_TRUE(*board1 != *board3);
	EXPECT_TRUE(*board1 != *board4);
	EXPECT_TRUE(*board2 != *board4);
	EXPECT_TRUE(*board3 != *board4);
}
TEST_F(BoardTest, Board_get_score_counter)
{
	Counter *c1 = board1->getScoreCounter();
	Counter *c2 = board2->getScoreCounter();
	Counter *c3 = board3->getScoreCounter();
	Counter *c4 = board4->getScoreCounter();
//check that c2 and c3 are the same
	EXPECT_TRUE(c2->getEmptyCells() == c3->getEmptyCells());
	EXPECT_TRUE(c2->getPlayer1Score() == c3->getPlayer1Score());
	EXPECT_TRUE(c2->getPlayer2Score() == c3->getPlayer2Score());
//check that c2 and c1 are the same except for empty cells.
	EXPECT_TRUE(c2->getEmptyCells() != c1->getEmptyCells());
	EXPECT_TRUE(c2->getPlayer1Score() == c1->getPlayer1Score());
	EXPECT_TRUE(c2->getPlayer2Score() == c1->getPlayer2Score());
//check that c3 and c1 are the same except for empty cells.
	EXPECT_TRUE(c1->getEmptyCells() != c3->getEmptyCells());
	EXPECT_TRUE(c1->getPlayer1Score() == c3->getPlayer1Score());
	EXPECT_TRUE(c1->getPlayer2Score() == c3->getPlayer2Score());
//compare c4 to c1,c2,c3 - they are not the same except for player2 score (2)
	EXPECT_TRUE(c1->getEmptyCells() != c4->getEmptyCells());
	EXPECT_TRUE(c1->getPlayer1Score() != c4->getPlayer1Score());
	EXPECT_TRUE(c1->getPlayer2Score() == c4->getPlayer2Score());
	EXPECT_TRUE(c2->getEmptyCells() != c4->getEmptyCells());
	EXPECT_TRUE(c2->getPlayer1Score() != c4->getPlayer1Score());
	EXPECT_TRUE(c2->getPlayer2Score() == c4->getPlayer2Score());
	EXPECT_TRUE(c3->getEmptyCells() != c4->getEmptyCells());
	EXPECT_TRUE(c3->getPlayer1Score() != c4->getPlayer1Score());
	EXPECT_TRUE(c3->getPlayer2Score() == c4->getPlayer2Score());
}
TEST_F(BoardTest, Board_get_player_min_max_move)
{
//the pair returned is : <best_move_index,best_move_score>
//player_2 the AI have 2 options (3,3) & (3,1) AI should choose (3,1).
	std::pair<int, int> expected = std::make_pair(1, 2);
	/* get all of player_2 possible moves for board 4 , his possible moves are
	 *  (3,3) & (3,1) minimax algorithm should return the (3,1)-> max opponent score 3.
	 *  while (3,3)-> max opponent score 5. */
	std::vector<std::vector<GamePiece> > board4_player_2_possible_moves =
			board4->getPlayerPossibleMoves(player_2, player_1);
	std::pair<int, int> returned = board4->getPlayerMinMaxMove(
			board4_player_2_possible_moves);
	EXPECT_EQ(expected.first, returned.first);
	EXPECT_EQ(expected.second, returned.second);
	/*  if (3,3) is chosen in the next move the player_1 can get to the same
	 score as player_2 but if (3,1) is chosen the best score player_1 can choose
	 still puts player_2 on top*/
	EXPECT_NE(0, returned.first);
	EXPECT_NE(0, returned.second);
}
TEST_F(BoardTest, Board_create_board_with_moves)
{
	std::vector<std::pair<Point, int> > play_moves;
	play_moves.push_back(std::make_pair(Point(3, 2), player_1));
	play_moves.push_back(std::make_pair(Point(1, 3), player_2));
	play_moves.push_back(std::make_pair(Point(0, 3), player_1));
	board3 = board3->createBoardWithMoves(play_moves, board3);
	board2 = board2->createBoardWithMoves(play_moves, board2);
	board1 = board1->createBoardWithMoves(play_moves, board1);
	EXPECT_TRUE(*board3 == *board2);
	EXPECT_TRUE(*board3 == *board4);
	EXPECT_TRUE(*board4 == *board2);
	EXPECT_TRUE(*board1 != *board2);
	EXPECT_TRUE(*board1 != *board3);
	EXPECT_TRUE(*board1 != *board4);
}
TEST_F(BoardTest, Board_get_player_possible_moves)
{
	std::vector<std::vector<GamePiece> > expected_player_2_possible_moves;
	std::vector<GamePiece> move0;
	std::vector<GamePiece> move1;
	GamePiece piece(3, 6, player_1);
	move0.push_back(GamePiece(Point(1, 1), player_2));
	move0.push_back(GamePiece(Point(2, 2), player_1));
	move0.push_back(GamePiece(Point(3, 3), empty));
	move1.push_back(GamePiece(Point(1, 1), player_2));
	move1.push_back(GamePiece(Point(2, 1), player_1));
	move1.push_back(GamePiece(Point(3, 1), empty));
	move1.push_back(GamePiece(Point(1, 3), player_2));
	move1.push_back(GamePiece(Point(2, 2), player_1));
	move1.push_back(GamePiece(Point(3, 1), empty));
	expected_player_2_possible_moves.push_back(move0);
	expected_player_2_possible_moves.push_back(move1);
	std::vector<std::vector<GamePiece> > board4_player_2_possible_moves =
			board4->getPlayerPossibleMoves(player_2, player_1);
	// check that both vectors and their strike vectors are the same
	EXPECT_EQ(board4_player_2_possible_moves.size(),
			expected_player_2_possible_moves.size());
	EXPECT_EQ(board4_player_2_possible_moves[0].size(),
			expected_player_2_possible_moves[0].size());
	EXPECT_EQ(board4_player_2_possible_moves[1].size(),
			expected_player_2_possible_moves[1].size());
	//compare all the gamePiece in both strike vectors by their symbol and point.
	if (board4_player_2_possible_moves.size()
			== expected_player_2_possible_moves.size())
	{
		for (std::size_t i = 0; i < board4_player_2_possible_moves.size(); i++)
		{
			std::vector<GamePiece> board4_current =
					board4_player_2_possible_moves[i];
			std::vector<GamePiece> expected_current =
					expected_player_2_possible_moves[i];
			if (board4_current.size() == expected_current.size())
			{
				for (std::size_t j = 0; j < board4_current.size(); j++)
				{
					EXPECT_TRUE(
							board4_current[j].getPieceLocationPoint()
									== expected_current[j].getPieceLocationPoint());
					EXPECT_EQ(board4_current[j].getGamePieceSymbol(),
							expected_current[j].getGamePieceSymbol());
					EXPECT_FALSE(
							piece.getPieceLocationPoint()
									== expected_current[j].getPieceLocationPoint());
					EXPECT_FALSE(
							piece.getPieceLocationPoint()
									== board4_current[j].getPieceLocationPoint());
				}
			}
		}
	}
}

TEST_F(BoardTest, Board_flip_all_Pieces_In_Range)
{
	std::vector<GamePiece> x_move_1;
	std::vector<GamePiece> o_move_1;
	std::vector<GamePiece> x_move_2;
	x_move_1.push_back(GamePiece(Point(1, 2), player_1));
	x_move_1.push_back(GamePiece(Point(2, 2), player_2));
	x_move_1.push_back(GamePiece(Point(3, 2), empty));
	o_move_1.push_back(GamePiece(Point(1, 1), player_2));
	o_move_1.push_back(GamePiece(Point(1, 2), player_1));
	o_move_1.push_back(GamePiece(Point(1, 3), empty));
	x_move_2.push_back(GamePiece(Point(2, 1), player_1));
	x_move_2.push_back(GamePiece(Point(1, 2), player_2));
	x_move_2.push_back(GamePiece(Point(0, 3), empty));
	board2->flipAllPiecesInRange(x_move_1);
	EXPECT_FALSE(*board2 == *board4);
	board2->flipAllPiecesInRange(o_move_1);
	EXPECT_FALSE(*board2 == *board4);
	board2->flipAllPiecesInRange(x_move_2);
	EXPECT_TRUE(*board2 == *board4);
	EXPECT_FALSE(*board2 == *board3);
}
TEST_F(BoardTest, Board_get_move_index)
{
	Point p1(4, 2);
	Point p2(2, 2);
	GamePiece piece1(p1, player_1);
	GamePiece piece2(44, 22, player_1);
	GamePiece piece3(2, 4, player_1);
	GamePiece piece4(3, 6, player_1);
	GamePiece piece5(5, 2, player_1);
	GamePiece piece6(52, 0, player_1);
	vector<GamePiece> vec1;
	vector<GamePiece> vec2;
	vector<vector<GamePiece> > vec3;
	vec1.push_back(piece2);
	vec1.push_back(piece3);
	vec1.push_back(piece4);
	vec1.push_back(piece1);
	vec2.push_back(piece5);
	vec2.push_back(piece6);
	vec3.push_back(vec1);
	vec3.push_back(vec2);
	EXPECT_EQ(0, board1->getMoveIndex(vec3, p1));
	EXPECT_EQ(1, board1->getMoveIndex(vec3, Point(52, 0)));
	EXPECT_EQ(-1, board1->getMoveIndex(vec3, p2));
	cout << endl << endl;
}
