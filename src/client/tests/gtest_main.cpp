/*
 * gtest_main.cpp
 *
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

 #include "gtest/gtest.h"
 GTEST_API_ int main(int argc, char **argv)
 {
 testing::InitGoogleTest(&argc, argv);
 return (RUN_ALL_TESTS());
 }


