/*
 * test_Point.cpp
 * 
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/Point.h"
#include <iostream>
using namespace std;

class PointTest: public ::testing::Test
{
	protected:
		Point p1;
		Point p2;
		Point p3;
		Point p4;
		Point p5;
		Point *p6;

		virtual void SetUp()
		{
			cout << "setting up...";
			cout << "finished setting up." << endl;
		}

		virtual void TearDown()
		{
			cout << "tearing down...";
			delete p6;
			cout << "finished tearing down." << endl;
		}

	public:
		PointTest() :
				p1(4, 2), p2(3, 6), p3(4, 2), p4(0, 0), p5(-11, -123)
		{
			p6 = new Point(-11, -123);
		}
};

TEST_F(PointTest, SettingAndGettingValues)
{
	// p1.
	EXPECT_EQ(4, p1.getRow());
	EXPECT_EQ(2, p1.getColumn());
	// p2.
	EXPECT_EQ(3, p2.getRow());
	EXPECT_EQ(6, p2.getColumn());
	// p3.
	EXPECT_EQ(4, p3.getRow());
	EXPECT_EQ(2, p3.getColumn());
	// p4.
	EXPECT_EQ(0, p4.getRow());
	EXPECT_EQ(0, p4.getColumn());
	// p5.
	EXPECT_EQ(-11, p5.getRow());
	EXPECT_EQ(-123, p5.getColumn());
	// p6.
	EXPECT_EQ(-11, p6->getRow());
	EXPECT_EQ(-123, p6->getColumn());
}

TEST_F(PointTest, PointToStringTest)
{
	EXPECT_EQ("(4,2)", p1.pointToString());
	EXPECT_EQ("(3,6)", p2.pointToString());
	EXPECT_EQ("(4,2)", p3.pointToString());
	EXPECT_EQ("(0,0)", p4.pointToString());
	EXPECT_EQ("(-11,-123)", p5.pointToString());
	EXPECT_EQ("(-11,-123)", p6->pointToString());
}

TEST_F(PointTest, EquelSignTest)
{
	EXPECT_TRUE(p1 == p3);
	EXPECT_TRUE(p5 == *p6);
	EXPECT_FALSE(p5 == p1);
	EXPECT_FALSE(p5 == p4);
	EXPECT_FALSE(p2 == *p6);
}

TEST_F(PointTest, NotEquelSignTest)
{
	EXPECT_FALSE(p1 != p3);
	EXPECT_FALSE(p5 != *p6);
	EXPECT_TRUE(p5 != p1);
	EXPECT_TRUE(p5 != p4);
	EXPECT_TRUE(p2 != *p6);
}

TEST_F(PointTest, CopyConstructor)
{
	Point copy1(p1);
	EXPECT_TRUE(p1 == copy1);

	Point copy2(*p6);
	EXPECT_TRUE(*p6 == copy2);

	Point *copy3 = new Point(p1);
	EXPECT_TRUE(p1 == *copy3);

	Point *copy4 = new Point(*p6);
	EXPECT_TRUE(*p6 == *copy4);

	delete copy3;
	delete copy4;
	cout << endl << endl;

}
