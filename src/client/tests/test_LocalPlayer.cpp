/*
 * test_LocalPlayer.cpp
 *
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/LocalPlayer.h"
#include <iostream>
using namespace std;

class LocalPlayerTest: public ::testing::Test
{
	protected:
		LocalPlayer nonset;
		LocalPlayer * pointerNonset;
		LocalPlayer preset;
		virtual void SetUp()
		{
			cout << "setting up...";
			preset.setPlayerTurn(true);
			cout << "finished setting up." << endl;
		}

		virtual void TearDown()
		{
			cout << "tearing down...";
			delete pointerNonset;
			cout << "finished tearing down." << endl;
		}

	public:
		LocalPlayerTest() :
				nonset(player_1), preset(player_2)
		{
			pointerNonset = new LocalPlayer(player_1);
		}
		;
};
TEST_F(LocalPlayerTest, GettingLocalPlayer_PlayerSymbol)
{
	EXPECT_EQ(player_1, nonset.getPlayerSymbol());
	EXPECT_EQ(player_1, pointerNonset->getPlayerSymbol());
	EXPECT_EQ(player_2, preset.getPlayerSymbol());

}
TEST_F(LocalPlayerTest, GettingLocalPlayer_OpponentSymbol)
{
	EXPECT_EQ(player_2, nonset.getOpponentSymbol());
	EXPECT_EQ(player_2, pointerNonset->getOpponentSymbol());
	EXPECT_EQ(player_1, preset.getOpponentSymbol());
}
TEST_F(LocalPlayerTest, GettingLocalPlayer_Turn_)
{
	EXPECT_FALSE(nonset.isPlayerTurn());
	EXPECT_FALSE(pointerNonset->isPlayerTurn());
	EXPECT_TRUE(preset.isPlayerTurn());
}
TEST_F(LocalPlayerTest, SettingLocalPlayer_Turn)
{
	nonset.setPlayerTurn(true);
	pointerNonset->setPlayerTurn(true);
	preset.setPlayerTurn(true);
	EXPECT_TRUE(nonset.isPlayerTurn());
	EXPECT_TRUE(pointerNonset->isPlayerTurn());
	EXPECT_TRUE(preset.isPlayerTurn());
}
TEST_F(LocalPlayerTest, LocalPlayer_CopyConstructor)
{
	LocalPlayer copy1(preset);
	EXPECT_EQ(preset.isPlayerTurn(), copy1.isPlayerTurn());
	EXPECT_EQ(preset.getPlayerSymbol(), copy1.getPlayerSymbol());
	EXPECT_EQ(preset.getOpponentSymbol(), copy1.getOpponentSymbol());
	copy1.setPlayerTurn(!preset.isPlayerTurn());
	EXPECT_EQ(!preset.isPlayerTurn(), copy1.isPlayerTurn());

	LocalPlayer copy2(nonset);
	EXPECT_EQ(nonset.isPlayerTurn(), copy2.isPlayerTurn());
	EXPECT_EQ(nonset.getPlayerSymbol(), copy2.getPlayerSymbol());
	EXPECT_EQ(nonset.getOpponentSymbol(), copy2.getOpponentSymbol());
	copy2.setPlayerTurn(!nonset.isPlayerTurn());
	EXPECT_EQ(!nonset.isPlayerTurn(), copy2.isPlayerTurn());

	LocalPlayer copy3(*pointerNonset);
	EXPECT_EQ(pointerNonset->isPlayerTurn(), copy3.isPlayerTurn());
	EXPECT_EQ(pointerNonset->getPlayerSymbol(), copy3.getPlayerSymbol());
	EXPECT_EQ(pointerNonset->getOpponentSymbol(), copy3.getOpponentSymbol());
	copy3.setPlayerTurn(!pointerNonset->isPlayerTurn());
	EXPECT_EQ(!pointerNonset->isPlayerTurn(), copy3.isPlayerTurn());

	LocalPlayer *copy4 = new LocalPlayer(preset);
	EXPECT_EQ(copy4->isPlayerTurn(), preset.isPlayerTurn());
	EXPECT_EQ(copy4->getPlayerSymbol(), preset.getPlayerSymbol());
	EXPECT_EQ(copy4->getOpponentSymbol(), preset.getOpponentSymbol());
	copy4->setPlayerTurn(!preset.isPlayerTurn());
	EXPECT_EQ(!preset.isPlayerTurn(), copy4->isPlayerTurn());
	delete copy4;
	cout << endl << endl;
}
