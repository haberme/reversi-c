/*
 * Server.cpp
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#include "../include/Server.h"

using namespace std;
#define MAX_CONNECTED_CLIENTS (10)

Server::Server(const int& port) :
		port_(port), server_socket_(0)
{
	cout << "Server: Starting Server.." << endl;
}

void Server::start()
{
	//Create a socket point
	setServerSocket(socket(AF_INET, SOCK_STREAM, 0));
	if (getServerSocket() == -1)
	{
		throw "Error: in Server::start failed to open server socket.";
	}
	//Assign a local address to the socket
	struct sockaddr_in serverAddress;
	//Initialize serverAddress.
	memset((void *) &serverAddress, 0, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_addr.s_addr = INADDR_ANY;
	serverAddress.sin_port = htons(getPort());
	if (bind(getServerSocket(), (struct sockaddr*) &serverAddress,
			sizeof(serverAddress)) == -1)
	{
		throw "Error: in Server::start failed to bind server socket.";
	}
	//Start listening to incoming connections
	listen(getServerSocket(), MAX_CONNECTED_CLIENTS);
	//Define the client`s socket`s structures
	struct sockaddr_in client_1_address;
	struct sockaddr_in client_2_address;
	socklen_t client_1_address_len;
	socklen_t client_2_address_len;
	int client_1_socket;
	int client_2_socket;
	while (true)
	{
		try
		{
			cout << "Server: waiting for client`s connections" << endl;
			//Accept the new client`s connections
			client_1_socket = accept(getServerSocket(),
					(struct sockaddr *) &client_1_address,
					&client_1_address_len);
			if (client_1_socket == -1)
			{
				throw "Error: in Server::start failed to accept client_1 socket.";
			}
			cout
					<< "Server: client_1 connected , waiting for client_2 connection."
					<< endl;
			/* first connecting client is being notified of his connection. */
			writeToClient(client_1_socket, client_1);
			client_2_socket = accept(getServerSocket(),
					(struct sockaddr *) &client_2_address,
					&client_2_address_len);
			if (client_2_socket == -1)
			{
				throw "Error: in Server::start failed to accept client_2 socket.";
			}
			cout << "Server: client_2 connected ,starting game." << endl;
			/* second connecting client is being notified of his connection. */
			writeToClient(client_2_socket, client_2);
			writeToClient(client_1_socket, have_move);
			int run1 = success;
			int run2 = success;
			do
			{
				if (isGameOver(run1, run2))
				{
					cout << "Server: server got Game Over signal from client."
							<< endl;
					break;
				}
				run1 = handleClients(client_1_socket, client_2_socket);
				if (isGameOver(run1, run2))
				{
					cout << "Server: server got Game Over signal from client."
							<< endl;
					break;
				}
				run2 = handleClients(client_2_socket, client_1_socket);
			} while ((run1 != failure) && (run2 != failure));
			/* close communication with the client`s */
			cout << "Server: closing clients sockets." << endl;
			close(client_1_socket);
			close(client_2_socket);
		} catch (const char *msg)
		{
			cout << "Server: closing clients sockets, reason: " << msg << endl;
			close(client_1_socket);
			close(client_2_socket);
		}
	}
	stop();
}
bool Server::isGameOver(int& run1, int&run2)
{
	bool is_game_over = false;
	//no move for both break and closing game
	if ((run1 == no_move) && (run2 == no_move))
	{
		is_game_over = true;
	}
	//got game over signal from one of the clients
	if ((run1 == game_over) || (run2 == game_over))
	{
		is_game_over = true;
	}
	return (is_game_over);
}
int Server::handleClients(const int &client_a_socket,
		const int &client_b_socket)
{
	int client_a_move_row;
	int client_a_move_column;
	int client_b_write_row;
	int client_b_write_column;
	int check_status_1;
	int check_status_2;
	int check_status_3;
	int check_status_4;
	//after every read/write we check the returned status from socket.
	/* read the move played by client_a */
	client_a_move_row = readFromClient(client_a_socket);
	check_status_1 = checkReadStatus(client_a_move_row);
	client_a_move_column = readFromClient(client_a_socket);
	check_status_2 = checkReadStatus(client_a_move_column);
	/* write the move played by client_a to client b*/
	client_b_write_row = writeToClient(client_b_socket, client_a_move_row);
	check_status_3 = checkReadStatus(client_b_write_row);
	client_b_write_column = writeToClient(client_b_socket,
			client_a_move_column);
	check_status_4 = checkReadStatus(client_b_write_column);
	if (check_status_1 != success)
	{
		return (check_status_1);
	}
	if (check_status_2 != success)
	{
		return (check_status_1);
	}
	if (check_status_3 != success)
	{
		return (check_status_1);
	}
	if (check_status_4 != success)
	{
		return (check_status_1);
	}
	return (success);
}
int Server::checkReadStatus(const int& read_data)
{
	// if got game over signal stop current game.
	if (read_data == game_over)
	{
		return (game_over);
	}
	if (read_data < 0)
	{
		return (failure);
	}
	if (read_data == no_move)
	{
		return (no_move);
	}
	return (success);
}
int Server::readFromClient(const int &client_socket) const
{
	int read_result;
	//read from client
	int n = read(client_socket, &read_result, sizeof(read_result));
	if (n == -1)
	{
		throw "Error: in Server::readFromClient failed reading from client socket";
		return (error);
	}
	if (n == 0)
	{
		throw "Error: in Server::readFromClient failed reading from client socket, client disconnected";
		return (disconnected);
	}
	return (read_result);
}
int Server::writeToClient(const int &client_socket, const int &data_to_write)
{
	/* write to the given client the given data. */
	int n = write(client_socket, &data_to_write, sizeof(data_to_write));
	if (n == -1)
	{
		throw "Error: in Server::writeToClient failed writing to client socket";
		return (error);
	}
	if (n == 0)
	{
		throw "Error: in Server::writeToClient failed writing to client socket, client disconnected";
		return (disconnected);
	}
	return (success);
}

int& Server::getPort()
{
	return (port_);
}

void Server::setPort(const int& port)
{
	port_ = port;
}

int& Server::getServerSocket()
{
	return (server_socket_);
}

void Server::setServerSocket(const int& serverSocket)
{
	server_socket_ = serverSocket;
}

void Server::stop()
{
	close(server_socket_);
}
