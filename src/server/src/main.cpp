#include "../include/Server.h"
#include "../include/IPPortReader.h"
#include <iostream>
#include <stdlib.h>
using namespace std;

int main(int argc, char const *argv[]) {
    try {
    	// get the ip and port from the file
    	struct IPPortSettings settings;
	const char* path="server_config.txt";
    	settings = readFile(path);
    	Server server(settings.port);
        server.start();
    } catch (const char *msg) {
        cout << "Cannot start server. Reason: " << msg << endl;
        exit(-1);
    }
}
