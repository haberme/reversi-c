/*
 * SocketConnectionStatus.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */
#ifndef SOCKETCONNECTIONSTATUS_H_
#define SOCKETCONNECTIONSTATUS_H_
enum ConnectionStatus
{
	disconnected = -90,
	connected = 90,
	error = -80,
	client_1 = 1,
	client_2 = 2,
	no_move = 50,
	have_move = 60,
	player_turn = 70,
	not_player_turn = 80,
	success = 100,
	failure = -100,
	game_over=150
};

#endif /* SOCKETCONNECTIONSTATUS_H_ */
