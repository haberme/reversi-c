/*
 * Server.h
 *
 *      Author: Doron_Norani           ID:305419020
 *      Author: Eran_Avigdor_Haberman  ID:201508793
 */

#ifndef SERVER_H_
#define SERVER_H_
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include "SocketConnectionStatus.h"

class Server
{
	public:
		/**
		 *Function Name: Server
		 *Description: construct a new server with given port number.
		 *Parameters:port the server port ,needed to communicate with server.
		 *Return Value: void.
		 */
		Server(const int& port);
		/**
		 *Function Name: start
		 *Description: starts the server, accepts 2 clients at a time.
		 *Parameters:void.
		 *Return Value: void.
		 */
		void start();

	private:
		int port_;
		int server_socket_; // the socket's file descriptor
		/**
		 *Function Name: isGameOver
		 *Description: checks if one of the client sent game over signal.
		 *Parameters:run1 - the returned value for handleClients(client 1,client 2)
		 *Parameters:run2 - the returned value for handleClients(client 2,client 1).
		 *Return Value: true if server got game over signal false otherwise.
		 */
		bool isGameOver(int& run1, int&run2);
		/**
		 *Function Name: checkReadStatus
		 *Description: reads data from client_a and sends it to client_b.
		 *Parameters:read_data - the data to check status for.
		 *Return Value: a value representing the read/write status (failure/success/no_move/game_over).
		 */
		int checkReadStatus(const int& read_data);

		/**
		 *Function Name: writeToClient
		 *Description: reads data from client_a and sends it to client_b.
		 *Parameters:client_a_socket - the client to read from.
		 *Parameters:client_b_socket - the client to write to.
		 *Return Value: a value representing the read/write status (failure/success/no_move/game_over).
		 */
		int handleClients(const int &client_a_socket,
				const int &client_b_socket);
		/**
		 *Function Name: writeToClient
		 *Description: sends given data to given client .
		 *Parameters:data_to_write - the data we want to send to client.
		 *Parameters:client_socket - needed to communicate with client.
		 *Return Value: a value representing the write status (error/disconnected/success/game_over/no_move).
		 */
		int writeToClient(const int &client_socket, const int &data_to_write);
		/**
		 *Function Name: readFromClient
		 *Description: reads data from given client .
		 *Parameters:client_socket - the client to read from.
		 *Return Value: a value representing the data sent from client or
		 * the read status (error/disconnected).
		 */
		int readFromClient(const int &client_socket) const;
		/**
		 *Function Name: getPort
		 *Description: returns this server port.
		 *Parameters: void.
		 *Return Value: server_Port - the server port .
		 */
		int& getPort();
		/**
		 *Function Name: setPort
		 *Description: sets this server port to given value.
		 *Parameters:port- the server port .
		 *Return Value: void.
		 */
		void setPort(const int& port);
		/**
		 *Function Name: getServerSocket
		 *Description: returns this server socket.
		 *Parameters: void.
		 *Return Value: server_socket_ - the server socket.
		 */
		int& getServerSocket();
		/**
		 *Function Name: setServerSocket
		 *Description: sets this server socket to given value.
		 *Parameters:serverSocket- the server socket to set.
		 *Return Value: void.
		 */
		void setServerSocket(const int& serverSocket);
		/**
		 *Function Name: stop
		 *Description: stop this server.
		 *Parameters:void.
		 *Return Value: void.
		 */
		void stop();
};
#endif /* CLIENT_H_ */
