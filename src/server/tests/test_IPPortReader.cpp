/*
 * IPPortReaderTest.cpp
 *
 *Author: Doron_Norani          ID:305419020
 *Author:Eran_Avigdor_Haberman  ID:201508793
 */

#include "gtest/gtest.h"
#include "../include/IPPortReader.h"

TEST(IPPortReaderTest, CorrectValues)
{
	const char *correctTest = "127.0.0.1\n12345";
	struct IPPortSettings correctValues;
	correctValues.IP = "127.0.0.1";
	correctValues.port = 12345;

	ASSERT_NO_THROW(readFile(correctTest));

	struct IPPortSettings testValues = readFile(correctTest);
	EXPECT_EQ(correctValues.IP, testValues.IP);
	EXPECT_EQ(correctValues.port, testValues.port);
}

TEST(IPPortReaderTest, CorruptIP)
{
	// IP first place.
	const char *IPFirstAbove = "3456.0.0.1\n12345";
	const char *IPFirstBelow = "-123.0.0.1\n12345";
	const char *IPFirstNotNumber = "127s.0.0.1\n12345";
	ASSERT_THROW(readFile(IPFirstAbove), IPError);
	ASSERT_THROW(readFile(IPFirstBelow), IPError);
	ASSERT_THROW(readFile(IPFirstNotNumber), IPError);

	// IP second place.
	const char *IPSecondAbove = "127.555.0.1\n12345";
	const char *IPSecondBelow = "127.-12.0.1\n12345";
	const char *IPSecondNotNumber = "127.wee.0.1\n12345";
	ASSERT_THROW(readFile(IPSecondAbove), IPError);
	ASSERT_THROW(readFile(IPSecondBelow), IPError);
	ASSERT_THROW(readFile(IPSecondNotNumber), IPError);

	// IP third place.
	const char *IPThirdAbove = "127.0.4242.1\n12345";
	const char *IPThirdBelow = "127.0.-42.1\n12345";
	const char *IPThirdNotNumber = "127.0.book.1\n12345";
	ASSERT_THROW(readFile(IPThirdAbove), IPError);
	ASSERT_THROW(readFile(IPThirdBelow), IPError);
	ASSERT_THROW(readFile(IPThirdNotNumber), IPError);

	// IP fourth place.
	const char *IPFourthAbove = "127.0.0.345\n12345";
	const char *IPFourthBelow = "127.0.0.-41\n12345";
	const char *IPFourthNotNumber = "127.0.0.1d\n12345";
	ASSERT_THROW(readFile(IPFourthAbove), IPError);
	ASSERT_THROW(readFile(IPFourthBelow), IPError);
	ASSERT_THROW(readFile(IPFourthNotNumber), IPError);
}

TEST(IPPortReaderTest, MissingIP)
{
	const char *IPMissing1 = "\n12345";
	const char *IPMissing2 = "12345";
	const char *IPMissing3 = "127.0.0.0.1\n12345";
	const char *IPMissing4 = "12345\n";
	const char *IPMissing5 = "127.0.1\n12345";
	const char *IPMissing6 = "127..0.1\n12345";

	ASSERT_THROW(readFile(IPMissing1), IPError);
	ASSERT_THROW(readFile(IPMissing2), IPError);
	ASSERT_THROW(readFile(IPMissing3), IPError);
	ASSERT_THROW(readFile(IPMissing4), IPError);
	ASSERT_THROW(readFile(IPMissing5), IPError);
	ASSERT_THROW(readFile(IPMissing6), IPError);
}

TEST(IPPortReaderTest, CorruptPort)
{
	const char *portAbove = "127.0.0.1\n99999";
	const char *portBelow = "127.0.0.1\n80";
	const char *portNegative = "127.0.0.1\n-80";
	const char *portNotNumber = "127.0.0.1\n1d234";

	ASSERT_THROW(readFile(portAbove), PortError);
	ASSERT_THROW(readFile(portBelow), PortError);
	ASSERT_THROW(readFile(portNegative), PortError);
	ASSERT_THROW(readFile(portNotNumber), PortError);
}

TEST(IPPortReaderTest, MissingPort)
{
	const char *portMissing1 = "127.0.0.1";
	const char *portMissing2 = "127.0.0.1\n";

	ASSERT_THROW(readFile(portMissing1), PortError);
	ASSERT_THROW(readFile(portMissing2), PortError);
}

TEST(StringToNumberTest, CorrectValues)
{
	EXPECT_EQ(42, stringToNumber("42"));
	EXPECT_EQ(-1, stringToNumber("4-2"));
	EXPECT_EQ(-1, stringToNumber("-"));
	EXPECT_EQ(-1, stringToNumber(""));
}
